package de.sollder1.messanger.conn;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import de.sollder1.messanger.conn.model.*;
import de.sollder1.messanger.crypt.RsaKeypair;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.List;

public class Connector {

    private static final TypeReference<List<ChatPartnerState>> CHAT_PARTNERS = new TypeReference<>() {
    };
    private static final TypeReference<List<ChatMessagePayload>> CHAT_MESSAGES = new TypeReference<>() {
    };
    private static Connector instance;

    public static Connector getInstance() {
        if (instance == null) {
            instance = new Connector("https://900m7.backend.sollder1.de/messanger/rest/");
        }
        return instance;
    }

    private final String serverBaseUri;
    private final HttpClient client = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(5))
            .build();
    private LoginResponsePayload loginContext;

    public Connector(String serverBaseUri) {
        this.serverBaseUri = serverBaseUri;
    }

    public void login(String username, String password) throws IOException, InterruptedException {
        LoginRequestPayload payload = getLoginRequestPayload(username, password);
        var request = baseBuilder("auth")
                .POST(HttpRequest.BodyPublishers.ofString(JsonMapperPool.getMapper().writeValueAsString(payload)))
                .build();
        loginContext = getResult(request, LoginResponsePayload.class);
    }

    public void register(String username, String password) throws IOException, InterruptedException {
        LoginRequestPayload payload = getLoginRequestPayload(username, password);
        var request = baseBuilder("accounts/me")
                .POST(HttpRequest.BodyPublishers.ofString(JsonMapperPool.getMapper().writeValueAsString(payload)))
                .build();
        getResult(request);
    }

    private LoginRequestPayload getLoginRequestPayload(String username, String password) {
        LoginRequestPayload payload = new LoginRequestPayload();
        payload.setUsername(username);
        payload.setPassword(password);
        return payload;
    }

    public List<ChatPartnerState> getContacts() throws IOException, InterruptedException {
        var request = baseBuilder("chatpartner").GET().build();
        return getResult(request, CHAT_PARTNERS);
    }

    public List<ChatMessagePayload> loadChat(ChatPartnerState partner) throws IOException, InterruptedException {
        var request = baseBuilder(String.format("chatpartner/%s/chat-messages", partner.getId())).GET().build();
        return getResult(request, CHAT_MESSAGES);
    }

    public ChatMessagePayload sendMessage(String partnerId, byte[] message) throws IOException, InterruptedException {
        var payload = new ChatMessagePayload();
        payload.setMessageEncrypted(message);
        var request = baseBuilder(String.format("chatpartner/%s/chat-messages", partnerId))
                .POST(HttpRequest.BodyPublishers.ofString(JsonMapperPool.getMapper().writeValueAsString(payload))).build();
        return getResult(request, ChatMessagePayload.class);
    }

    public void sendRsaKeyToChatpartner(ChatPartnerState chatPartner, RsaKeypair.PublicKey publicKey) throws IOException, InterruptedException {
        chatPartner.setStatus(ChatPartnerState.Status.RSA_KEY_AVAILABLE);
        chatPartner.setPublicRsaKey(JsonMapperPool.getMapper().writeValueAsString(publicKey));
        var request = baseBuilder("chatpartner")
                .PUT(HttpRequest.BodyPublishers.ofString(JsonMapperPool.getMapper().writeValueAsString(chatPartner))).build();
        getResult(request);
    }

    public void sendSymmetricKeyToChatpartner(ChatPartnerState chatPartner, byte[] encryptedSymmetricKey) throws IOException, InterruptedException {
        chatPartner.setStatus(ChatPartnerState.Status.SYMMETRIC_KEY_AVAILABLE);
        chatPartner.setSymmetricKeyEncrypted(encryptedSymmetricKey);
        var request = baseBuilder("chatpartner")
                .PUT(HttpRequest.BodyPublishers.ofString(JsonMapperPool.getMapper().writeValueAsString(chatPartner))).build();
        getResult(request);
    }

    public void sendKeyConfirmed(ChatPartnerState chatPartner) throws IOException, InterruptedException {
        chatPartner.setStatus(ChatPartnerState.Status.ACTIVE);
        var request = baseBuilder("chatpartner")
                .PUT(HttpRequest.BodyPublishers.ofString(JsonMapperPool.getMapper().writeValueAsString(chatPartner))).build();
        getResult(request);
    }

    public void postNewChatPartner(String username) throws IOException, InterruptedException {
        ChatPartnerState chatPartner = new ChatPartnerState();
        AccountPayload account = new AccountPayload();
        account.setUsername(username);
        chatPartner.setOtherUser(account);
        var request = baseBuilder("chatpartner")
                .POST(HttpRequest.BodyPublishers.ofString(JsonMapperPool.getMapper().writeValueAsString(chatPartner))).build();
        getResult(request);
    }

    private EmptyPayload getResult(HttpRequest request) throws IOException, InterruptedException {
        client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        return new EmptyPayload();
    }

    private <T extends BaseResponsePayload> T getResult(HttpRequest request, Class<T> targetClass) throws IOException, InterruptedException {
        var result = client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        return JsonMapperPool.getMapper().readValue(result, targetClass);
    }

    private <T> T getResult(HttpRequest request, TypeReference<T> reference) throws IOException, InterruptedException {
        var result = client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        return JsonMapperPool.getMapper().readValue(result, reference);
    }

    private HttpRequest.Builder baseBuilder(String subPath) {
        return HttpRequest.newBuilder()
                .uri(URI.create(serverBaseUri + subPath))
                .header("Authorization", loginContext != null ? loginContext.getToken() : "")
                .timeout(Duration.ofSeconds(10));
    }

    public LoginResponsePayload getLoginContext() {
        return loginContext;
    }


}
