package de.sollder1.messanger.conn.model;




public class ChatMessagePayload extends BaseResponsePayload {
    private AccountPayload sender;
    private byte[] messageEncrypted;

    public AccountPayload getSender() {
        return sender;
    }

    public void setSender(AccountPayload sender) {
        this.sender = sender;
    }

    public byte[] getMessageEncrypted() {
        return messageEncrypted;
    }

    public void setMessageEncrypted(byte[] messageEncrypted) {
        this.messageEncrypted = messageEncrypted;
    }
}
