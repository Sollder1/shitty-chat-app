package de.sollder1.messanger.conn.model;

public class ChatPartnerState extends BaseResponsePayload {

    private AccountPayload otherUser;
    private Status status;
    private boolean initiator;
    private String publicRsaKey;
    private byte[] symmetricKeyEncrypted;

    private int newMessages;

    public String getPublicRsaKey() {
        return publicRsaKey;
    }

    public void setPublicRsaKey(String publicRsaKey) {
        this.publicRsaKey = publicRsaKey;
    }

    public byte[] getSymmetricKeyEncrypted() {
        return symmetricKeyEncrypted;
    }

    public void setSymmetricKeyEncrypted(byte[] symmetricKeyEncrypted) {
        this.symmetricKeyEncrypted = symmetricKeyEncrypted;
    }

    public AccountPayload getOtherUser() {
        return otherUser;
    }

    public void setOtherUser(AccountPayload otherUser) {
        this.otherUser = otherUser;
    }


    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public boolean isInitiator() {
        return initiator;
    }

    public void setInitiator(boolean initiator) {
        this.initiator = initiator;
    }

    public int getNewMessages() {
        return newMessages;
    }

    public void setNewMessages(int newMessages) {
        this.newMessages = newMessages;
    }

    public void incrementNewMessages() {
        newMessages++;
    }

    public void resetNewMessages() {
        newMessages = 0;
    }

    public enum Status {
        REQUESTED, RSA_KEY_AVAILABLE, SYMMETRIC_KEY_AVAILABLE, ACTIVE
    }

    @Override
    public String toString() {
        return otherUser.getUsername() + "(" + newMessages + ")";
    }
}
