package de.sollder1.messanger.conn;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;

public class JsonMapperPool {

    private static final JsonMapper mapper = new JsonMapper();
    private static final JsonMapper mapperUnwrapRoot = new JsonMapper();

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapperUnwrapRoot.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapperUnwrapRoot.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);

    }

    public static JsonMapper getMapper() {
        return mapper;
    }

    public static JsonMapper getMapperUnwrapRoot() {
        return mapperUnwrapRoot;
    }

}
