package de.sollder1.messanger.ui;

import de.sollder1.messanger.conn.Connector;
import de.sollder1.messanger.conn.JsonMapperPool;
import de.sollder1.messanger.conn.model.ChatPartnerState;
import de.sollder1.messanger.crypt.RsaHelper;
import de.sollder1.messanger.crypt.RsaKeypair;
import de.sollder1.messanger.crypt.mmcp.SymmetricEncryption;
import de.sollder1.messanger.notif.UpdateMessage;
import de.sollder1.messanger.notif.WebsocketClient;
import de.sollder1.messanger.storage.StorageApi;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    public TextArea messageInput;
    @FXML
    private ListView<ChatPartnerState> contactList;
    @FXML
    private Menu usernameMenu;
    @FXML
    private VBox chatMessages;
    @FXML
    private ScrollPane chatMessagesScrollContainer;

    @Override
    public synchronized void initialize(URL url, ResourceBundle resourceBundle) {
        WebsocketClient.registerMainController(this);
        chatMessages.heightProperty().addListener(observable -> chatMessagesScrollContainer.setVvalue(1D));
        var username = Connector.getInstance().getLoginContext().getUsername();
        usernameMenu.setText(username);
        loadContacts();
    }

    private synchronized void loadContacts() {
        try {
            contactList.getItems().clear();
            contactList.getItems().addAll(Connector.getInstance().getContacts());
            if (!contactList.getItems().isEmpty()) {
                var toSelect = contactList.getItems().get(0);
                var selected = contactList.getSelectionModel().getSelectedItems();
                if (selected.size() == 1) {
                    toSelect = selected.get(0);
                }
                contactList.getSelectionModel().select(toSelect);
                onSelect(toSelect);
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    public synchronized void onContactSelected() {
        getSelectedItem().ifPresent(this::onSelect);
    }

    private void onSelect(ChatPartnerState chatPartner) {
        if (chatPartner.getStatus() == ChatPartnerState.Status.ACTIVE) {
            this.loadChat(chatPartner);
        } else {
            this.loadInactivePanel(chatPartner);
        }
    }

    private void loadInactivePanel(ChatPartnerState chatPartner) {
        chatMessages.getChildren().clear();

        if (chatPartner.getStatus() == ChatPartnerState.Status.REQUESTED) {
            handleRequestedState(chatPartner);
        } else if (chatPartner.getStatus() == ChatPartnerState.Status.RSA_KEY_AVAILABLE) {
            handleRsaKeyAvailable(chatPartner);
        } else if (chatPartner.getStatus() == ChatPartnerState.Status.SYMMETRIC_KEY_AVAILABLE) {
            handleSymmetricKeyAvailable(chatPartner);
        } else {
            chatMessages.getChildren().add(new Label("Geblockt!"));
        }
    }

    private void handleRequestedState(ChatPartnerState chatPartner) {
        if (chatPartner.isInitiator()) {
            chatMessages.getChildren().add(new Label("Warte auf Schlüsseltausch"));
        } else {
            var button = new Button("Asymmetrischen Schlüsseltausch vorbereiten.");
            button.setOnMouseClicked(e -> handleSendRsaKey(chatPartner));
            chatMessages.getChildren().add(button);
        }
    }

    private void handleRsaKeyAvailable(ChatPartnerState chatPartner) {
        if (chatPartner.isInitiator()) {
            var button = new Button("Symmetrischen Schlüssel senden.");
            button.setOnMouseClicked(e -> handleSendSymmetricKey(chatPartner));
            chatMessages.getChildren().add(button);
        } else {
            chatMessages.getChildren().add(new Label("Warte auf Schlüsseltausch"));
        }
    }

    private void handleSymmetricKeyAvailable(ChatPartnerState chatPartner) {
        if (chatPartner.isInitiator()) {
            chatMessages.getChildren().add(new Label("Warte auf Schlüsseltausch bestätigung."));
        } else {
            var button = new Button("Schlüsseltausch bestätigen.");
            button.setOnMouseClicked(e -> handleSendConfirmKeyExchange(chatPartner));
            chatMessages.getChildren().add(button);
        }
    }

    private void handleSendRsaKey(ChatPartnerState chatPartner) {
        var keypair = RsaHelper.getKeyPair();
        StorageApi.saveKeyPair(chatPartner.getId(), keypair);
        try {
            Connector.getInstance().sendRsaKeyToChatpartner(chatPartner, keypair.getPublicKey());
            loadContacts();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleSendSymmetricKey(ChatPartnerState chatPartner) {
        //TODO: vllt. müssen wir uns  den PublicKey in Zukunft doch merken...
        try {
            var key = JsonMapperPool.getMapper().readValue(chatPartner.getPublicRsaKey(), RsaKeypair.PublicKey.class);
            var symmetricKey = SymmetricEncryption.getNewKey();
            StorageApi.saveSymmetricKey(chatPartner.getId(), symmetricKey);
            Connector.getInstance().sendSymmetricKeyToChatpartner(chatPartner, RsaHelper.encrypt(symmetricKey, key));
            loadContacts();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    private void handleSendConfirmKeyExchange(ChatPartnerState chatPartner) {
        try {
            var privateKey = StorageApi.getKeypair(chatPartner.getId()).orElseThrow().getPrivateKey();
            var decryptedSymmetricKey = RsaHelper.decrypt(chatPartner.getSymmetricKeyEncrypted(), privateKey);
            StorageApi.saveSymmetricKey(chatPartner.getId(), decryptedSymmetricKey);
            Connector.getInstance().sendKeyConfirmed(chatPartner);
            loadContacts();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized void receivedMessage(UpdateMessage updateMessage) {
        contactList.getItems().stream().
                filter(partner -> partner.getId().equals(updateMessage.getPartnerId()))
                .findAny()
                .ifPresent(partner -> handleReceivedMessageForPartner(partner, updateMessage));
    }

    private void handleReceivedMessageForPartner(ChatPartnerState partner, UpdateMessage updateMessage) {
        var selected = getSelectedItem();
        if (selected.isPresent() && Objects.equals(partner.getId(), selected.get().getId())) {
            loadChat(partner);
        } else {
            partner.incrementNewMessages();
            contactList.refresh();
        }
    }


    private synchronized void loadChat(ChatPartnerState partner) {
        try {
            chatMessages.getChildren().clear();
            var data = Connector.getInstance().loadChat(partner);
            data.stream()
                    .map(payload -> new ChatMessageElement(payload, partner))
                    .forEach(message -> chatMessages.getChildren().add(message));
            partner.resetNewMessages();
            contactList.refresh();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private synchronized Optional<ChatPartnerState> getSelectedItem() {
        return contactList.getSelectionModel().getSelectedItems().stream().findFirst();
    }

    public synchronized void sendMessage() throws IOException, InterruptedException {
        var partner = getSelectedItem().orElseThrow();
        var key = StorageApi.getSymmetricKey(partner.getId()).orElseThrow();
        var message = SymmetricEncryption.encrypt(key, messageInput.getText().getBytes(StandardCharsets.UTF_8));
        Connector.getInstance().sendMessage(partner.getId(), message);
        messageInput.setText("");
        loadChat(partner);
    }

    public void onNewPartner() {
        TextInputDialog td = new TextInputDialog();
        td.setTitle("Ziel username eingeben");
        td.setHeaderText("Ziel username eingeben");
        td.showAndWait().ifPresent(this::handleAddPartner);
    }

    private void handleAddPartner(String username) {
        try {
            Connector.getInstance().postNewChatPartner(username);
            loadContacts();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void onReloadContacts() {
        loadContacts();
    }
}
