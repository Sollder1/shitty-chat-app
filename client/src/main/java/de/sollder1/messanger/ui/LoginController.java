package de.sollder1.messanger.ui;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.sollder1.messanger.Main;
import de.sollder1.messanger.conn.Connector;
import de.sollder1.messanger.notif.WebsocketHandler;
import jakarta.websocket.WebSocketContainer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;

import java.io.IOException;

public class LoginController {

    @FXML
    public TextField loginUser;
    @FXML
    public TextField loginPwd;

    @FXML
    protected void onLogin() throws IOException, InterruptedException {
        Connector.getInstance().login(loginUser.getCharacters().toString(), loginPwd.getCharacters().toString());
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/mainApp.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        scene.getStylesheets().add("style.css");
        Main.stage.setScene(scene);
        WebsocketHandler.connect();
    }

    @FXML
    protected void onRegister(ActionEvent clickEvent) throws IOException, InterruptedException {
        Connector.getInstance().register(loginUser.getCharacters().toString(), loginPwd.getCharacters().toString());
        onLogin();
    }
}
