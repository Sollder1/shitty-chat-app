package de.sollder1.messanger.ui;

import de.sollder1.messanger.conn.model.ChatMessagePayload;
import de.sollder1.messanger.conn.model.ChatPartnerState;
import de.sollder1.messanger.crypt.mmcp.SymmetricEncryption;
import de.sollder1.messanger.storage.StorageApi;
import javafx.geometry.Insets;
import javafx.scene.control.Label;

public class ChatMessageElement extends Label {

    public ChatMessageElement(ChatMessagePayload content, ChatPartnerState partner) {
        //TODO : get key
        var key = StorageApi.getSymmetricKey(partner.getId()).orElseThrow();
        var decrypted = SymmetricEncryption.decrypt(key, content.getMessageEncrypted());
        setText(new String(decrypted) + " (" + content.getSender().getUsername() + ")");
        getStyleClass().add("chat-message");
        setPadding(new Insets(10));
    }
}
