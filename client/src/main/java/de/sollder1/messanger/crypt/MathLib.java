package de.sollder1.messanger.crypt;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

public class MathLib {

    private static BigInteger one_m = new BigInteger("-1");
    private static BigInteger zero = new BigInteger("0");
    private static BigInteger one = new BigInteger("1");
    private static BigInteger two = new BigInteger("2");

    //Wrapper für modPow
    public static boolean primeTest(BigInteger n, int y) {

        if (bigger(n, zero) && equal(n.mod(two), zero)) {
            return false;
        }
        //Test:
        BigInteger d = n.subtract(one).divide(two);

        for (int i = 0; i < y; i++) {
            BigInteger a = getRandomA(n);
            if (bigger(ggt(a, n), one)) {
                return false;
            } else {
                BigInteger b = binaryExpoOwn(a, d, n);
                if (!equal(b, one) && !equal(b, one_m.mod(n))) {
                    return false;
                }
            }
        }
        return true;
    }

    public static BigInteger binaryExpo(BigInteger base, BigInteger expo, BigInteger m) {
        return base.modPow(expo, m);
    }

    //Generiert einen zufälliges a
    public static BigInteger binaryExpoOwn(BigInteger base, BigInteger expo, BigInteger m) {
        //1.
        BigInteger res = new BigInteger("1");
        //2.
        while (bigger(expo, zero)) {
            //2.a)
            if (equal(expo.mod(two), one)) {
                res = res.multiply(base).mod(m);
            }
            //2.b)
            base = base.multiply(base).mod(m);

            //2.c)
            expo = expo.divide(two);
        }
        //3.
        return res;
    }

    public static BigInteger getRandomA(BigInteger max) {

        Random rand = new Random();
        BigInteger result = new BigInteger(max.bitLength(), rand);

        while (result.compareTo(max) >= 0) {
            result = new BigInteger(max.bitLength(), rand);
        }

        //Nullen abfangen
        if (equal(result, zero)) {
            return one;
        }

        return result;
    }

    //BEstimmt den größten GEmiensamen Teiler
    public static BigInteger ggt(BigInteger a, BigInteger b) {
        while (b.compareTo(zero) > 0) {
            BigInteger rest = a.mod(b);
            a = b;
            b = rest;
        }
        return a;
    }

    public static BigInteger erwEukl(BigInteger e, BigInteger phiN) {

        var d = BigInteger.ONE;
        var u = BigInteger.ZERO;
        while (!equal(phiN, BigInteger.ZERO)) {
            var q = e.divide(phiN);
            var b1 = phiN;
            phiN = e.subtract(q.multiply(phiN));
            e = b1;
            var u1 = u; // Variable zum Zwischenspeichern
            u = d.subtract(q.multiply(u));
            d = u1;
        }
        return d;
    }


    public static BigInteger getPrime(int length) {
        return BigInteger.probablePrime(length, new SecureRandom());
    }

    private static boolean bigger(BigInteger a, BigInteger b) {
        return a.compareTo(b) > 0;
    }

    public static boolean equal(BigInteger a, BigInteger b) {
        return a.compareTo(b) == 0;
    }

}
