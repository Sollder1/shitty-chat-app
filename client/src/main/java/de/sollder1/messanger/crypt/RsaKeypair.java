package de.sollder1.messanger.crypt;

import java.math.BigInteger;

public final class RsaKeypair {
    private PublicKey publicKey;
    private PrivateKey privateKey;

    public RsaKeypair() {
    }

    public RsaKeypair(PublicKey publicKey, PrivateKey privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public static final class PublicKey {
        private BigInteger n;
        private BigInteger e;

        public PublicKey() {
        }

        public PublicKey(BigInteger n, BigInteger e) {
            this.n = n;
            this.e = e;
        }


        public BigInteger getN() {
            return n;
        }

        public void setN(BigInteger n) {
            this.n = n;
        }

        public BigInteger getE() {
            return e;
        }

        public void setE(BigInteger e) {
            this.e = e;
        }
    }

    public static final class PrivateKey {
        private BigInteger n;
        private BigInteger d;

        public PrivateKey() {
        }

        public PrivateKey(BigInteger n, BigInteger d) {
            this.n = n;
            this.d = d;
        }

        public BigInteger getN() {
            return n;
        }

        public void setN(BigInteger n) {
            this.n = n;
        }

        public BigInteger getD() {
            return d;
        }

        public void setD(BigInteger d) {
            this.d = d;
        }
    }


}
