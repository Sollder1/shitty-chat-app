package de.sollder1.messanger.crypt;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RsaHelper {

    private static final int BITS = 2048;
    private static final int BYTES = BITS / 8;


    public static RsaKeypair getKeyPair() {

        var p = MathLib.getPrime(BITS / 2);
        var q = MathLib.getPrime(BITS / 2);

        var n = p.multiply(q);
        var phi_n = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));

        //Hint vllt. nicht mal nötig ne prime zu machen...
        var e = MathLib.getPrime(phi_n.bitLength() - 10);
        var d = MathLib.erwEukl(e, phi_n);

        return new RsaKeypair(new RsaKeypair.PublicKey(n, e), new RsaKeypair.PrivateKey(n, d));

    }

    public static byte[] encrypt(byte[] clearData, RsaKeypair.PublicKey key) {

        List<Byte> bytes = new ArrayList<>();
        var limit = getLimit(clearData);

        for (int i = 0; i < limit; i++) {
            byte[] partialBytes = getPartialBytes(clearData, i);
            var clearAsNumber = new BigInteger(partialBytes);
            var encryptedAsNumber = MathLib.binaryExpo(clearAsNumber, key.getE(), key.getN());
            var encryptedBytes = encryptedAsNumber.toByteArray();
            for (byte encryptedByte : encryptedBytes) {
                bytes.add(encryptedByte);
            }
        }
        return mapByteListToArray(bytes);
    }

    public static byte[] decrypt(byte[] encryptedData, RsaKeypair.PrivateKey key) {

        List<Byte> bytes = new ArrayList<>();
        var limit = getLimit(encryptedData);

        for (int i = 0; i < limit; i++) {
            byte[] partialBytes = getPartialBytes(encryptedData, i);
            var encryptedAsNumber = new BigInteger(partialBytes);
            var decryptedAsNumber = MathLib.binaryExpo(encryptedAsNumber, key.getD(), key.getN());
            var decryptedBytes = decryptedAsNumber.toByteArray();
            for (byte decryptedByte : decryptedBytes) {
                bytes.add(decryptedByte);
            }
        }
        return mapByteListToArray(bytes);
    }

    private static byte[] mapByteListToArray(List<Byte> bytes) {
        byte[] result = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            result[i] = bytes.get(i);
        }
        return result;
    }

    private static byte[] getPartialBytes(byte[] encryptedData, int i) {
        var upper = (i + 1) * BYTES;

        if (encryptedData.length < (i + 1) * BYTES) {
            upper = encryptedData.length;
        }

        return Arrays.copyOfRange(encryptedData, i * BYTES, upper);
    }

    private static int getLimit(byte[] clearData) {
        if ((clearData.length % BYTES) == 0) {
            return clearData.length / BYTES;
        } else {
            return (clearData.length / BYTES) + 1;
        }
    }

    public static void main(String[] args) {
        var keypair = getKeyPair();
        for (int i = 1000; i < 1002; i++) {
            int leftLimit = 48; // numeral '0'
            int rightLimit = 122; // letter 'z'
            int targetStringLength = i;
            Random random = new Random();

            String generatedString = random.ints(leftLimit, rightLimit + 1)
                    .filter(j -> (j <= 57 || j >= 65) && (j <= 90 || j >= 97))
                    .limit(targetStringLength)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();

            var e = encrypt(generatedString.getBytes(StandardCharsets.UTF_8), keypair.getPublicKey());
            System.out.println(new String(decrypt(e, keypair.getPrivateKey())));
        }
    }

}
