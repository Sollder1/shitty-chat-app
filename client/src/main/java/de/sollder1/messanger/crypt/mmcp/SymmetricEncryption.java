package de.sollder1.messanger.crypt.mmcp;

import static de.sollder1.messanger.crypt.mmcp.CryptographicHash.hash;
import static de.sollder1.messanger.crypt.mmcp.Utility.copyArray;

/**
 * Major Mistakes Symmetric Encryption
 * a symmetric block cypher with 256 byte blocks
 * works best with 256 byte key
 *
 */
public final class SymmetricEncryption {
    private SymmetricEncryption() {
    }
    private static final byte[] permutedBytes = {114, -8, 18, -21, -17, 14, -91, 46, 27, -30, 38, 24, 2, 66, 97, 57, 87, 10, -101, -53, 49, -39, 111, 117, -103, 72, 19, -51, -116, 110, 41, -117, -5, -41, 21, -76, -78, 56, 100, -128, 42, 78, -125, -83, 39, 65, 60, 15, -59, -58, -102, 17, 43, -112, 11, -46, 94, 69, 28, -44, 74, -66, 113, 107, -124, 20, 3, 71, 32, -69, 48, -1, 8, -62, 116, 51, 119, 101, -109, 45, 59, 70, 26, 127, -2, -126, 77, -79, 47, -29, 79, -89, 34, -37, -65, 4, 12, -93, -106, 81, -47, -108, -100, -11, -113, 120, -36, -119, 104, -86, -61, 52, -20, 76, -77, 115, -10, -9, -98, 9, -127, -82, -40, 31, 35, -72, -48, 37, 64, -4, -81, -90, 106, 84, -27, 121, -120, -63, 75, 112, -107, 99, 61, -22, 54, -50, 25, -45, -57, 13, 73, -75, -55, 16, -92, -3, 58, -13, -87, 63, -121, 93, 23, -26, 98, 83, 55, -35, 62, 82, 36, -70, -111, -74, -7, 30, -95, -104, 95, 53, 7, -34, -85, 50, -24, 85, 68, 123, 33, -15, -114, -105, -23, -28, 1, 40, -54, 5, -110, -52, -80, -99, -19, 124, 126, 86, 105, 88, 67, -16, -88, -14, 103, -49, 80, -56, 92, -43, 122, -6, 0, -118, -60, 102, 118, -68, -12, 91, -33, 22, -97, -18, 96, 125, -31, 6, 109, 108, -64, 90, -96, -115, -71, 89, -73, -38, -122, -42, 44, -94, -25, 29, -84, -67, -32, -123};

    public static byte[] getNewKey() {  //2048-bit key as 256 byte array
        return Utility.secureRandomBytes(256);
    }

    public static byte[] encryptBlock(byte[] key, byte[] data, int rounds) {     //encrypts 256 byte block of data
        //if (data.length != 256){throw new Exception("Datablock for encrypting must be 256 bytes long but is " + data.length + " bytes long");}
        byte[] shiftedData = new byte[256];
        for (int round = 0; round < rounds; round++) {
            for (int i = 0; i < 256; i++) {
                shiftedData[i] = data[(i + (key[(key[round % key.length] - Byte.MIN_VALUE) % key.length] - Byte.MIN_VALUE)) % data.length ];     //shift bytes right depending on key
            }
            for (int i = 0; i < 256; i++) {
                shiftedData[i] = (byte) (shiftedData[i] ^ key[i % key.length]);     //xor data with key
                shiftedData[i] = (byte) (shiftedData[i] + ((key[(key[round % key.length] - Byte.MIN_VALUE) % key.length] - Byte.MIN_VALUE) % 256));        //add key to byte depending on key
            }
            for (int i = 0; i < 256; i++) {
                data[i] = shiftedData[permutedBytes[i]-Byte.MIN_VALUE];
            }
        }
        return data;
    }

    public static byte[] decryptBlock(byte[] key, byte[] data, int rounds) {     //decrypts 256 byte block of data
        //if (data.length != 256){throw new Exception("Datablock for decrypting must be 256 bytes long but is " + data.length + " bytes long");}
        byte[] shiftedData = new byte[256];
        for (int round = rounds - 1; round >= 0; round--){
            for (int i = 0; i < 256; i++) {
                shiftedData[permutedBytes[i]-Byte.MIN_VALUE] = data[i];      //unmix data using mixing Vector
            }
            for (int i = 0; i < 256; i++) {
                shiftedData[i] = (byte) (shiftedData[i] + (256 - (key[(key[round % key.length] - Byte.MIN_VALUE) % key.length] - Byte.MIN_VALUE) % 256));        //subtract key from byte depending on key
                shiftedData[i] = (byte) (shiftedData[i] ^ key[i % key.length]);       //xor data with key
            }
            for (int i = 0; i < 256; i++) {
                data[i] = shiftedData[(i + data.length * (-Byte.MIN_VALUE) - (key[(key[round % key.length] - Byte.MIN_VALUE) % key.length] - Byte.MIN_VALUE)) % data.length];        //shift bytes left depending on key
            }
        }
        return data;
    }

    public static byte[] encryptionHandler(byte[] key, byte[] data, int rounds) {
        byte[] encryptedData = new byte[(data.length / 256 + 1) * 256];
        System.arraycopy(data, 0, encryptedData, 0, data.length);
        byte[] secureRandom = Utility.secureRandomBytes(encryptedData.length - 1 - data.length);
        System.arraycopy(secureRandom, 0, encryptedData, data.length, encryptedData.length - 1 - data.length);        //add padding to encryption array
        encryptedData[encryptedData.length - 1] = (byte) (encryptedData.length - data.length + Byte.MIN_VALUE - 1);       //note how long padding is in last byte
        for (int block = 0; block < encryptedData.length / 256; block++) {      //encrypt per Block
            byte[] encryptedBlock = encryptBlock(key, Utility.copyArray(encryptedData, block * 256, (block + 1) * 256), rounds);
            System.arraycopy(encryptedBlock, 0, encryptedData, block * 256, 256);
        }
        return encryptedData;
    }

    public static byte[] decryptionHandler(byte[] key, byte[] encryptedData, int rounds) {
        byte[] data = Utility.copyArray(encryptedData);
        for (int block = 0; block < data.length / 256; block++) {      //decrypt per block
            byte[] decryptedBlock = decryptBlock(key, Utility.copyArray(data, block * 256, block * 256 + 256), rounds);
            System.arraycopy(decryptedBlock, 0, data, block * 256, 256);
        }
        return Utility.copyArray(data, 0, data.length - data[data.length - 1] + Byte.MIN_VALUE - 1);     //return only data without padding
    }

    public static byte[] encrypt(byte[] key, byte[] data) {
        int rounds = (256 * 16) / Math.min(256,key.length);
        if (key.length !=256){
            key = Utility.copyArray(hash(key));
        }
        return encryptionHandler(key, data, rounds);
    }

    public static byte[] decrypt(byte[] key, byte[] data) {
        int rounds = (256 * 16) / Math.min(256,key.length);
        if (key.length !=256){
            key = Utility.copyArray(hash(key));
        }
        return decryptionHandler(key, data, rounds);
    }
}



    /*
    public byte[] multithreadedEncryptionHandler(byte[] key, byte[] data, int rounds) {
        byte[] encryptedData = new byte[(data.length / 256 + 1) * 256];
        System.arraycopy(data,0,encryptedData,0,data.length);
        byte[] secureRandom = secureRandomBytes(encryptedData.length-1-data.length);
        System.arraycopy(secureRandom,0,encryptedData,data.length,encryptedData.length-1-data.length);        //add padding to encryption array
        encryptedData[encryptedData.length-1] = (byte)(encryptedData.length - data.length + Byte.MIN_VALUE - 1);       //note how long padding is in last byte
        for (int block = 0; block < encryptedData.length / 256; block++) {      //encrypt per Block
            Thread thread_1 = new multithreadBlockEncrypt(block,key,copyArrayRange(data,block*256,(block+1)*256),rounds);
            thread_1.start();
        }
        return encryptedData;
    }



    class multithreadBlockEncrypt extends Thread {
        private int block;
        private byte[] key;
        public byte[] data;
        private final int rounds;

        public multithreadBlockEncrypt(int block, byte[] key, byte[] data, int rounds) {
            this.block = block;
            this.key = key;
            this.data = data;
            this.rounds = rounds;
            System.arraycopy(this.data,0,data,block*256,256);
        }
        public void run() {
            for (int round = 0; round < rounds; round++) {
                byte[] shiftedData = new byte[256];
                for (int i = 0; i < 256; i++) {
                    shiftedData[i] = data[(i + (key[(key[round%key.length]-Byte.MIN_VALUE) % key.length] - Byte.MIN_VALUE)) % data.length];      //shift bytes right depending on key
                }
                for (int i = 0; i < 256; i++) {
                    shiftedData[i] = (byte) (shiftedData[i] ^ key[i % key.length]);     //xor data with key
                    shiftedData[i] = (byte) (shiftedData[i] + ((key[(key[round%key.length]-Byte.MIN_VALUE) % key.length] - Byte.MIN_VALUE)%256));        //add key to byte depending on key
                }
                data = shiftedData;
            }
        }
    }
     */
