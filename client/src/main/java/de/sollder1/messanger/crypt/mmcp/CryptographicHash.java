package de.sollder1.messanger.crypt.mmcp;

import java.util.Arrays;

/**
 * Major Mistakes Cryptographic Hash
 * returns a hashed 256 byte array from a byte array
 */
public class CryptographicHash {

    private CryptographicHash() {
    }

    public static byte[] hash(byte[] data) {
        byte[] permutatedBytes = {-107, -31, 100, -115, -63, 6, 93, 115, -55, -45, 56, -113, -121, 41, -60, 82, 32, 120, -49, -13, 31, 21, -116, -34, 104, 40, 103, -109, 67, -114, -37, 70, 24, 66, -100, 75, 57, 77, -29, 5, -102, 69, -5, -8, -22, 84, -108, -99, 47, 127, 50, -127, -51, -2, -3, -86, 105, -103, 35, 63, -20, -58, 87, 89, -25, 30, 126, -35, 102, -85, 94, 98, -91, -14, -17, -126, -97, 61, 95, 80, -76, 49, -118, 48, 22, -30, -72, -12, 96, -79, -43, -94, -40, 36, -9, 53, -71, 71, -69, 117, 2, 39, -117, -77, 106, -27, -19, 16, -7, -67, 51, -18, -73, -80, -50, -47, -98, -65, 65, -42, 55, 81, -1, 90, 52, 125, -4, 119, -92, -57, 76, -87, -93, 73, -78, 54, 79, 8, -101, -6, -23, -105, 44, -81, -90, 85, 0, -10, 3, 11, -24, -16, -123, 83, 17, 121, -119, 42, -28, -112, -104, -125, 109, -32, -122, 78, 12, -61, 60, 1, 10, 15, -36, 7, -21, -62, -88, -95, -56, -68, -124, -64, -83, 107, 46, 34, 14, -41, 13, 43, -120, 123, -38, -26, 92, 26, 59, 62, -33, 111, 19, -52, -89, 9, 118, -46, 4, -54, -39, -66, -59, -48, -74, 68, -106, 38, 101, 124, 72, -15, 33, 29, -82, 97, 28, 25, 108, 116, 88, -84, 114, -75, -44, 45, -110, -128, 27, 58, 112, 113, -96, -53, 122, -11, 74, 37, 18, 20, 99, 23, -111, 86, 91, 110, 64, -70};
        byte[] inputData = Utility.copyArray(data);
        if (inputData.length <= 128) {
            byte[] extendedData = new byte[inputData.length * 2];
            System.arraycopy(inputData, 0, extendedData, 0, inputData.length);
            for (int i = 0; i < inputData.length; i++) {
                extendedData[i + inputData.length] = (byte) (~inputData[i]);
            }
            inputData = extendedData;
        }
        for (int i = 256; i < inputData.length; i += 256) {
            permutatedBytes = SymmetricEncryption.decryptBlock(Utility.copyArray(inputData, Math.min(i, inputData.length - 256), Math.min(i + 256, inputData.length)), permutatedBytes, 524288 / inputData.length + 1);
        }

        return SymmetricEncryption.decryptBlock(inputData, permutatedBytes, 1048576 / (inputData.length + 2) + 1);

    }
}

