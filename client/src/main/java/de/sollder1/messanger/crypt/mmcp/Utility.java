package de.sollder1.messanger.crypt.mmcp;

import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Major Mistakes Utilities
 */

public class Utility {
    public static byte[] copyArray(byte[] array){
        byte[] copiedArray = new byte[array.length];
        System.arraycopy(array, 0, copiedArray, 0, array.length);
        return copiedArray;
    }
    public static byte[] copyArray(byte[] array, int from, int to) {
        byte[] croppedArray = new byte[to - from];
        System.arraycopy(array, from, croppedArray, 0, to - from);
        return croppedArray;
    }
    public static byte[] secureRandomBytes(int length) {        //returns secure random byte[] of defined length
        SecureRandom randbits = new SecureRandom();
        byte[] randbytes = new byte[length];
        randbits.nextBytes(randbytes);
        return randbytes;
    }

    public static byte[] newPermutatedBytes() {
        byte[] array = new byte[256];
        for (int i = 0; i < 256; i++) {
            array[i] = (byte) (i);
        }
        for (int i = 0; i < 30000; i++) {
            int x = secureRandomBytes(1)[0] - Byte.MIN_VALUE;
            int y = secureRandomBytes(1)[0] - Byte.MIN_VALUE;
            byte b = array[x];
            array[x] = array[y];
            array[y] = b;
        }
        return array;
    }

    public static byte[] getPermutatedBytes(){
        return permutatedBytes;
    }

    private static final byte[] permutatedBytes = {-85, 72, 29, -125, -21, 50, -9, -104, -117, 106, 101, -17, 21, -118, -75, 115, -122, 63, 71, -58, 67, -94, -1, 51, -57, -40, -126, 48, -124, 38, 86, -77, -32, 16, 0, 118, -69, -34, -65, 43, -79, -113, 74, 97, -100, 60, -60, -108, 5, -24, -127, -6, -35, 110, -29, 70, 47, 76, -95, 103, 53, -2, 35, 40, -87, 37, -70, -110, 24, -7, 1, 90, -4, 100, -15, 68, 4, 49, -3, -59, -106, 94, -107, -14, -45, -67, 9, 8, 91, -13, -115, 83, 127, -26, 55, -119, -120, -11, -47, 105, -83, -53, -42, 112, -128, -90, -93, 114, -25, -49, 31, 59, 111, -38, 36, -27, 122, -78, -51, 87, -80, 41, -92, -81, -111, 23, 42, 79, 95, 3, 92, -88, 93, 121, -28, -5, -46, 66, 89, -19, -89, -18, -68, 14, 46, 13, 65, 62, 104, -91, 33, 126, -84, 99, -12, -74, 11, 57, 116, -50, 27, -123, -48, -76, -102, 10, -36, -30, -10, -62, -43, 18, -55, 52, 22, -8, 39, -112, 26, 107, -16, 64, -63, -23, 81, -101, 17, 109, -116, -71, -56, 108, 123, 58, -64, 84, 28, -44, 54, -52, 77, 88, -121, 78, 125, 15, -82, 44, 6, -98, 45, 56, -33, -39, 30, -86, 7, 80, 69, -54, 117, 96, 102, -41, -66, 98, 113, 2, -20, -61, 32, 12, 85, -22, -73, 19, -103, 120, 61, 25, 75, 124, -72, 73, 34, 20, 119, -31, -109, -96, -105, -97, -99, 82, -114, -37};

    /*
    public static byte[] getPermutatedBytes(int i){
        return permutatedBytesMatrix[i];
    }
    */

}
