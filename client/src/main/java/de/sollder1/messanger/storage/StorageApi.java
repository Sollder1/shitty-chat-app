package de.sollder1.messanger.storage;

import com.fasterxml.jackson.core.type.TypeReference;
import de.sollder1.messanger.conn.Connector;
import de.sollder1.messanger.conn.JsonMapperPool;
import de.sollder1.messanger.crypt.RsaKeypair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Optional;

public final class StorageApi {


    private static final TypeReference<Map<String, RsaKeypair>> RSA_KEYS = new TypeReference<>() {
    };

    private static final TypeReference<Map<String, byte[]>> SYMMETRIC_KEYS = new TypeReference<>() {
    };


    private static final String RSA_FILE = "rsa-key.json";
    private static final String SYMMETRIC_FILE = "sym-key.json";

    private StorageApi() {
    }

    //RSA Stuff...
    public static void saveKeyPair(String id, RsaKeypair keypair) {
        try {
            var content = parseRsaFile();
            content.remove(id);
            content.put(id, keypair);
            saveFile(RSA_FILE, JsonMapperPool.getMapper().writeValueAsString(content));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Optional<RsaKeypair> getKeypair(String partnerId) {
        try {
            return Optional.ofNullable(parseRsaFile().get(partnerId));
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    private static Map<String, RsaKeypair> parseRsaFile() throws IOException {
        var content = getFileContent(RSA_FILE).orElse("{}");
        return JsonMapperPool.getMapper().readValue(content, RSA_KEYS);
    }

    //Symmetric keys:
    public static void saveSymmetricKey(String id, byte[] key) {
        try {
            var content = parseSymmetricKeyFile();
            content.remove(id);
            content.put(id, key);
            saveFile(SYMMETRIC_FILE, JsonMapperPool.getMapper().writeValueAsString(content));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Optional<byte[]> getSymmetricKey(String partnerId) {
        try {
            return Optional.ofNullable(parseSymmetricKeyFile().get(partnerId));
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    private static Map<String, byte[]> parseSymmetricKeyFile() throws IOException {
        var content = getFileContent(SYMMETRIC_FILE).orElse("{}");
        return JsonMapperPool.getMapper().readValue(content, SYMMETRIC_KEYS);
    }


    private static void saveFile(String filename, String newContent) throws IOException {
        var path = getBasePath().resolve(filename);
        if (!Files.exists(path)) {
            Files.createFile(path);
        }
        Files.write(path, newContent.getBytes(StandardCharsets.UTF_8), StandardOpenOption.WRITE);
    }

    private static Optional<String> getFileContent(String filename) throws IOException {
        var path = getBasePath().resolve(filename);
        if (!Files.exists(path)) {
            return Optional.empty();
        }
        return Optional.of(Files.readString(path, StandardCharsets.UTF_8));
    }

    private static Path getBasePath() throws IOException {
        var basePath = Paths.get(System.getProperty("user.home"), ".shitty-chat-app", Connector.getInstance().getLoginContext().getId());

        if (!Files.exists(basePath)) {
            Files.createDirectories(basePath);
        }
        return basePath;
    }


}
