package de.sollder1.messanger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    public static Stage stage;

    public static void main(String[] args) {
        //var keys = RsaHelper.getKeyPair();
        launch(args);
    }


    @Override
    public void start(Stage stage) throws IOException {
        Main.stage = stage;
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/login.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Shitty-Chat-App");
        stage.setScene(scene);
        stage.show();
    }





    /*
        var toTransmit = "Hello World!";
        System.out.printf("At start: '%s'\n", toTransmit);

        var encrypted = RsaHelper.encrypt(toTransmit.getBytes(StandardCharsets.UTF_8), keys.publicKey());
        //System.out.printf("Encrypted: '%s'\n", new String(encrypted, StandardCharsets.UTF_8));

        var decrypted = RsaHelper.decrypt(encrypted, keys.privateKey());
        System.out.printf("Decrypted: '%s'\n", new String(decrypted, StandardCharsets.UTF_8));

     */
}
