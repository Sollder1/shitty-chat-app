package de.sollder1.messanger.notif;

public class UpdateMessage {
    private String partnerId;
    private byte[] newMessage;
    private MessageType messageType;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public byte[] getNewMessage() {
        return newMessage;
    }

    public void setNewMessage(byte[] newMessage) {
        this.newMessage = newMessage;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public enum MessageType {
        NEW_MESSAGE
    }



}

