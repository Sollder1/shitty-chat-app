package de.sollder1.messanger.notif;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.sollder1.messanger.conn.JsonMapperPool;
import de.sollder1.messanger.ui.MainController;
import jakarta.websocket.*;
import javafx.application.Platform;

@ClientEndpoint
public class WebsocketClient {

    private static MainController CONTROLLER;

    public static void registerMainController(MainController controller) {
        CONTROLLER = controller;
    }

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Open");
    }

    @OnMessage
    public void onMessage(Session session, String message) throws JsonProcessingException {
        var updateMessage = JsonMapperPool.getMapper().readValue(message, UpdateMessage.class);
        this.handleMessage(updateMessage);

        System.out.println(message);
    }

    private synchronized void handleMessage(UpdateMessage updateMessage) {
        switch (updateMessage.getMessageType()) {
            case NEW_MESSAGE:
                Platform.runLater(() -> CONTROLLER.receivedMessage(updateMessage));

            default:
                System.out.println("Received unknown message... Ignoring!");
        }
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        System.out.printf("Close %s - %s %n", reason.getReasonPhrase(), reason.getCloseCode());
        WebsocketHandler.connect();
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        System.out.println("Error");
        throwable.printStackTrace();
        WebsocketHandler.connect();

    }

}
