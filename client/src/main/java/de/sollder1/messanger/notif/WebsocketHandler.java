package de.sollder1.messanger.notif;

import de.sollder1.messanger.conn.Connector;
import jakarta.websocket.ContainerProvider;
import jakarta.websocket.DeploymentException;
import jakarta.websocket.WebSocketContainer;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public final class WebsocketHandler {

    private static final WebSocketContainer container;

    static {
        container = ContainerProvider.getWebSocketContainer();
        container.setDefaultMaxSessionIdleTimeout(0);
    }

    private WebsocketHandler() {

    }

    public static void connect() {
        try {
            String uri = String.format("wss://900m7.backend.sollder1.de/messanger/notifications/%s",
                    Connector.getInstance().getLoginContext().getToken());
            WebsocketClient client = new WebsocketClient();
            container.connectToServer(client, new URI(uri)).setMaxIdleTimeout(0);
        } catch (DeploymentException | URISyntaxException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
