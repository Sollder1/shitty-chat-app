module de.sollder {
    exports de.sollder1.messanger;
    exports de.sollder1.messanger.ui;
    exports de.sollder1.messanger.conn.model;
    exports de.sollder1.messanger.notif;
    exports de.sollder1.messanger.crypt;
    opens de.sollder1.messanger.conn.model;
    opens de.sollder1.messanger.ui;
    requires javafx.controls;
    requires javafx.fxml;
    requires java.net.http;
    requires com.fasterxml.jackson.databind;
    requires jakarta.websocket.client;
}