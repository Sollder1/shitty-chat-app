package de.sollder1.messanger.mmcp;

import de.sollder1.messanger.crypt.mmcp.Utility;
import org.junit.Assert;
import org.junit.Test;

import static de.sollder1.messanger.crypt.mmcp.CryptographicHash.hash;
import static de.sollder1.messanger.crypt.mmcp.Utility.secureRandomBytes;


public class CryptographicHashTest {

    @Test
    public void hashTest_Performance_1GB(){
        int keylength = 1000000000;
        byte[] data = secureRandomBytes(keylength);
        Assert.assertEquals(256, hash(data).length);
    }

    @Test
    public void hashTest_Performance_256Byte(){
        int keylength = 256;
        byte[] data = secureRandomBytes(keylength);
        Assert.assertEquals(256, hash(data).length);
    }

    @Test
    public void hashTest_Performance_1Byte(){
        int keylength = 1;
        byte[] data = secureRandomBytes(keylength);
        Assert.assertEquals(256, hash(data).length);
    }

    @Test
    public void hashTest_Length() {
        for (int i = 1; i < 700; i++) {
            byte[] data = secureRandomBytes(i);
            Assert.assertEquals(256, hash(data).length);
        }
    }

    @Test
    public void hashTest_Correctness(){
        final byte[] testData = secureRandomBytes(8);
        final byte[] originalHash = hash(testData);
        //System.out.print("original: "); for (byte n : originalHash){System.out.print(n-Byte.MIN_VALUE+ " ");};System.out.println();
        for (int i = 1; i < 100; i++) {
            byte[] controllHash = hash(testData);
            //System.out.print("control: "); for (byte n : controllHash){System.out.print(n-Byte.MIN_VALUE+" ");};System.out.println();
            Assert.assertEquals(originalHash[0],controllHash[0]);
            Assert.assertArrayEquals(originalHash, controllHash);
        }
    }

    @Test
    public void hashTest_CorrectnesOnInputLength(){
        for (int i = 1; i < 1000; i++) {
            byte[] testData = secureRandomBytes(i);
            byte[] hash1 = Utility.copyArray(hash(testData));
            //System.out.print("hash1: "); for (byte n : hash1){System.out.print(n-Byte.MIN_VALUE+ " ");};System.out.println();
            byte[] hash2 = Utility.copyArray(hash(testData));
            //System.out.print("hash2: "); for (byte n : hash1){System.out.print(n-Byte.MIN_VALUE+ " ");};System.out.println();
            Assert.assertEquals(hash1[0],hash2[0]);
            Assert.assertArrayEquals(hash1, hash2);
        }
    }

    @Test
    public void hashTest_Collision(){
        final byte[] originalData = secureRandomBytes(32);
        final byte[] orignialHash = Utility.copyArray(hash(originalData));
        for (int i = 1; i < 100; i++) {
            byte[] collisionHash = Utility.copyArray(hash(secureRandomBytes(32)));
            for (int j = 0; j < 255; j++) {
                if (orignialHash[j] != collisionHash[j]){
                    break;
                }
                Assert.assertNotEquals(orignialHash[255],collisionHash[255]);
            }
        }
    }

}