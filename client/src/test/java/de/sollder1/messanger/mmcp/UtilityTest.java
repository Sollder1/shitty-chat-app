package de.sollder1.messanger.mmcp;

import org.junit.Assert;
import org.junit.Test;

import static de.sollder1.messanger.crypt.mmcp.SymmetricEncryption.getNewKey;
import static de.sollder1.messanger.crypt.mmcp.Utility.*;
import static java.util.Arrays.copyOfRange;

public class UtilityTest {

    @Test
    public void copyArrayRangeTest() {
        byte[] data = getNewKey();
        for (int i = 0; i < data.length; i++) {
            Assert.assertEquals(copyOfRange(data,i,data.length).length, copyArray(data,i,data.length).length);
            Assert.assertArrayEquals(copyOfRange(data,i,data.length), copyArray(data,i,data.length));
        }
    }

    @Test
    public void secureRandomBytesTest() {
        Assert.assertEquals(255, secureRandomBytes(255).length);
        Assert.assertEquals(300, secureRandomBytes(300).length);
        Assert.assertEquals(1, secureRandomBytes(1).length);
        Assert.assertEquals(0,secureRandomBytes(0).length);
    }

    @Test
    public void newPermutatedBytesTest() {
        for (int n = 0; n < 256; n++) {
            byte[] array = newPermutatedBytes();
            for (int i = 0; i < 256; i++) {
                for (int j = i + 1; j < 256; j++) {
                    Assert.assertNotEquals(array[i], array[j]);
                }
            }
            //System.out.print("{"); for (int i = 0; i < array.length - 1; i++) {System.out.print(array[i] + ", ");}System.out.println(array[255] + "}");
        }
    }

    @Test
    public void getPermutatedBytesTest() {
        byte[] permutedBytes = getPermutatedBytes();
        for (int i = Byte.MIN_VALUE; i<=Byte.MAX_VALUE; i++){
            int counter = 0;
            for (int j = 0; j<256; j++){
                if (permutedBytes[j] == i){
                    counter++;
                }
            }
            Assert.assertEquals(1,counter);
        }
    }
    /*
    @Test
    public void getPermutatedBytesMatrixTest() {
        for (int i = 0; i<256; i++){
            for (int j = i+1; j<256; j++){
                Assert.assertArrayEquals(getPermutatedBytes(i),getPermutatedBytes(j));
            }
        }
    }
    */
}