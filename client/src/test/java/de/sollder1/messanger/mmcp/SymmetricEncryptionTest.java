package de.sollder1.messanger.mmcp;


import org.junit.Assert;
import org.junit.Test;


import static de.sollder1.messanger.crypt.mmcp.SymmetricEncryption.*;
import static de.sollder1.messanger.crypt.mmcp.Utility.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;


public class SymmetricEncryptionTest {

    private static final byte[] performanceTestKey = getNewKey();
    private static final byte[] performanceTestData = secureRandomBytes(100000000);
    @Test
    public void encryptionPerformanceTest(){
        encrypt(performanceTestKey,performanceTestData);
    }

    @Test
    public void decryptionPerformanceTest(){
        decrypt(performanceTestKey,performanceTestData);
    }

    @Test
    public void cryptPerformanceTest(){
        byte[] encryptetTestMessgae = encrypt(performanceTestKey,performanceTestData);
        byte[] decryptetTestMessgae = decrypt(performanceTestKey,encryptetTestMessgae);

        Assert.assertArrayEquals(performanceTestData,decryptetTestMessgae);
    }


    @Test
    public void getNewKeyTest() {
        byte[] key = getNewKey();
        Assert.assertEquals(256, key.length);
    }

    @Test
    public void encryptBlockTest_EncryptedLength() {
        for (int i = 0; i < 10000; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(256);
            byte[] encryptedBlock = encryptBlock(key, data,i);
            Assert.assertEquals(256, encryptedBlock.length);
        }
    }

    @Test
    public void encryptBlockTest_Correctness() {
        byte[] key = getNewKey();
        byte[] data = secureRandomBytes(256);
        for (int j = 2; j < 400; j++) {
            byte[] dataCopy = copyArray(data);
            byte[] encryptedBlock1 = encryptBlock(key, dataCopy, j);
            byte[] encryptedBlock1Copy = copyArray(encryptedBlock1);
            //System.out.print("block1: "); for (byte n : encryptedBlock1Copy){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
            for (int i = 0; i < 300; i++) {
                dataCopy = copyArray(data);
                byte[] encryptedBlock2 = encryptBlock(key, dataCopy, j);
                //System.out.print("block2: "); for (byte n : encryptedBlock2){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
                Assert.assertArrayEquals(encryptedBlock1Copy, encryptedBlock2);
            }
        }
    }

    @Test
    public void decryptBlockTest() {
        for (int i = 0; i < 5000; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(256);
            byte[] dataCopy = copyArray(data);
            //System.out.print("input:  "); for (byte n : dataCopy){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
            byte[] encryptedBlock = encryptBlock(key, data,i);
            byte[] decryptedBlock = decryptBlock(key, encryptedBlock,i);
            //System.out.print("output: "); for (byte n : decryptedBlock){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
            Assert.assertArrayEquals(dataCopy, decryptedBlock);
        }
    }

    @Test
    public void encryptionHandlerTest() {
        for (int i = 0; i < 256; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(i);
            byte[] encryptedData = encryptionHandler(key, data, 256);
            Assert.assertEquals(256, encryptedData.length);
        }
        for (int i = 256; i < 512; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(i);
            byte[] encryptedData = encryptionHandler(key, data, 256);
            Assert.assertEquals(512, encryptedData.length);
        }
        for (int i = 512; i < 512+256; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(i);
            byte[] encryptedData = encryptionHandler(key, data, 256);
            Assert.assertEquals(512+256, encryptedData.length);
        }
    }

    @Test
    public void decryptionHandlerTest() {
        for (int i = 0; i < 10000; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(i);
            byte[] dataCopy = copyArray(data);
            //System.out.print("input:  "); for (byte n : data){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
            byte[] encryptedData = encryptionHandler(key, data, 32);
            byte[] decryptedData = decryptionHandler(key, encryptedData, 32);
            //System.out.print("output: "); for (byte n : decryptedData){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
            assertEquals(dataCopy.length, decryptedData.length);
            assertArrayEquals(dataCopy, decryptedData);
        }
    }

    @Test
    public void decryptionHandler_Keylength_Test() {
        for (int j = 1; j < 400; j++) {
            //System.out.println(j);
            byte[] key = secureRandomBytes(j);
            for (int i = 0; i < 500; i++) {
                byte[] data = secureRandomBytes(i);
                byte[] dataCopy = copyArray(data);
                //System.out.print("input:  "); for (byte n : dataCopy){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
                byte[] encryptedData = encryptionHandler(key, data, 8);
                byte[] decryptedData = decryptionHandler(key, encryptedData, 8);
                //System.out.print("output: "); for (byte n : decryptedData){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
                Assert.assertEquals(dataCopy.length, decryptedData.length);
                Assert.assertArrayEquals(dataCopy, decryptedData);
            }
        }
    }

    @Test
    public void encryptTest() {
        for (int i = 0; i < 256; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(i);
            byte[] encryptedData = encryptionHandler(key, data, 256);
            Assert.assertEquals(256, encryptedData.length);
        }
        for (int i = 256; i < 512; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(i);
            byte[] encryptedData = encryptionHandler(key, data, 256);
            Assert.assertEquals(512, encryptedData.length);
        }
        for (int i = 512; i < 512+256; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(i);
            byte[] encryptedData = encryptionHandler(key, data, 256);
            Assert.assertEquals(512+256, encryptedData.length);
        }
    }

    @Test
    public void decryptTest() {
        for (int i = 0; i < 10000; i++) {
            byte[] key = getNewKey();
            byte[] data = secureRandomBytes(i);
            byte[] dataCopy = copyArray(data);
            //System.out.print("input:  "); for (byte n : dataCopy){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
            byte[] encryptedData = encrypt(key, data);
            byte[] decryptedData = decrypt(key, encryptedData);
            //System.out.print("output: "); for (byte n : decryptedData){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
            Assert.assertEquals(dataCopy.length, decryptedData.length);
            Assert.assertArrayEquals(dataCopy, decryptedData);
        }
    }

    @Test
    public void decryptionTest_Keylength() {
        for (int j = 1; j < 300; j += 3) {
            //System.out.println("Keylength = " + j);
            byte[] key = secureRandomBytes(j);
            for (int i = 0; i < 700; i+=99) {
                byte[] data = secureRandomBytes(i);
                byte[] dataCopy = copyArray(data);
                //System.out.print("input:  "); for (byte n : dataCopy){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
                byte[] encryptedData = encrypt(key, data);
                byte[] decryptedData = decrypt(key, encryptedData);
                //System.out.print("output: "); for (byte n : decryptedData){System.out.print(n-Byte.MIN_VALUE+" ");}System.out.println();
                Assert.assertEquals(dataCopy.length, decryptedData.length);
                Assert.assertArrayEquals(dataCopy, decryptedData);
            }
        }
    }
}
