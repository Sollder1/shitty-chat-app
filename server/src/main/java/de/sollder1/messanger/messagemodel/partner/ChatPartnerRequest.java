package de.sollder1.messanger.messagemodel.partner;

import de.sollder1.base.core.processing.messagemodel.request.RestRequest;
import de.sollder1.base.plugins.account.messagemodel.AccountPayload;
import lombok.Getter;
import lombok.Setter;


public class ChatPartnerRequest extends RestRequest<ChatPartnerPayload> {

}
