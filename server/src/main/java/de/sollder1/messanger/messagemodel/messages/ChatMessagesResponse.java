package de.sollder1.messanger.messagemodel.messages;

import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import de.sollder1.messanger.messagemodel.partner.ChatPartnerPayload;

import java.util.List;


public class ChatMessagesResponse extends RestResponse<List<ChatMessagePayload>> {
    public ChatMessagesResponse() {
    }

    public ChatMessagesResponse(List<ChatMessagePayload> payload) {
        super(payload);
    }
}
