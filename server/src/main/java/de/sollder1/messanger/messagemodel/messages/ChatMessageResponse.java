package de.sollder1.messanger.messagemodel.messages;

import de.sollder1.base.core.processing.messagemodel.response.RestResponse;


public class ChatMessageResponse extends RestResponse<ChatMessagePayload> {
    public ChatMessageResponse() {
    }

    public ChatMessageResponse(ChatMessagePayload payload) {
        super(payload);
    }
}
