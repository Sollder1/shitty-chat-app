package de.sollder1.messanger.messagemodel.messages;

import de.sollder1.base.core.processing.messagemodel.response.BaseResponsePayload;
import de.sollder1.base.plugins.account.messagemodel.AccountPayload;
import de.sollder1.messanger.messagemodel.partner.ChatPartnerPayload;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatMessagePayload extends BaseResponsePayload {
    private AccountPayload sender;
    private byte[] messageEncrypted;
}
