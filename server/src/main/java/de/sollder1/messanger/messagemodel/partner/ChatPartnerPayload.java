package de.sollder1.messanger.messagemodel.partner;

import de.sollder1.base.core.processing.messagemodel.response.BaseResponsePayload;
import de.sollder1.base.plugins.account.messagemodel.AccountPayload;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatPartnerPayload extends BaseResponsePayload {

    private AccountPayload otherUser;
    private boolean initiator;
    private String status;
    private String publicRsaKey;
    private byte[] symmetricKeyEncrypted;
}
