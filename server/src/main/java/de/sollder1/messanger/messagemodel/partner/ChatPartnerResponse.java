package de.sollder1.messanger.messagemodel.partner;

import de.sollder1.base.core.processing.messagemodel.request.RestRequest;
import de.sollder1.base.core.processing.messagemodel.response.RestResponse;


public class ChatPartnerResponse extends RestResponse<ChatPartnerPayload> {
    public ChatPartnerResponse() {
    }

    public ChatPartnerResponse(ChatPartnerPayload payload) {
        super(payload);
    }
}
