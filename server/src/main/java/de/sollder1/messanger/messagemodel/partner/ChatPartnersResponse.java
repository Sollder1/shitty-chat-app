package de.sollder1.messanger.messagemodel.partner;

import de.sollder1.base.core.processing.messagemodel.response.RestResponse;

import java.util.List;


public class ChatPartnersResponse extends RestResponse<List<ChatPartnerPayload>> {
    public ChatPartnersResponse() {
    }

    public ChatPartnersResponse(List<ChatPartnerPayload> payload) {
        super(payload);
    }
}
