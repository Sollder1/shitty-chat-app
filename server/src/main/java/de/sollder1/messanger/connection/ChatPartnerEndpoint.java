package de.sollder1.messanger.connection;

import de.sollder1.base.core.connection.BasicEndpoint;
import de.sollder1.messanger.processors.partner.GetAllChatPartnerProcessor;
import de.sollder1.messanger.processors.partner.PostChatPartnerProcessor;
import de.sollder1.messanger.processors.partner.PutChatPartnerProcessor;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("chatpartner")
public class ChatPartnerEndpoint extends BasicEndpoint {

    @POST
    public Response post(String payload) {
        return handle(payload, PostChatPartnerProcessor.class);
    }

    @PUT
    public Response put(String payload) {
        return handle(payload, PutChatPartnerProcessor.class);
    }

    @GET
    public Response getAll() {
        return handle(GetAllChatPartnerProcessor.class);
    }
}
