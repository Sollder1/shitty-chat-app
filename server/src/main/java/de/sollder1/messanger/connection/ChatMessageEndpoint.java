package de.sollder1.messanger.connection;

import de.sollder1.base.core.connection.BasicEndpoint;
import de.sollder1.base.core.processing.messagemodel.response.BaseResponsePayload;
import de.sollder1.base.plugins.account.messagemodel.AccountPayload;
import de.sollder1.messanger.processors.messages.GetAllChatMessagesProcessor;
import de.sollder1.messanger.processors.messages.PostChatMessageProcessor;
import de.sollder1.messanger.processors.partner.GetAllChatPartnerProcessor;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import lombok.Getter;
import lombok.Setter;

@Path("chatpartner/{chatpartner-id}/chat-messages")
public class ChatMessageEndpoint extends BasicEndpoint {
    @GET
    public Response getAll() {
        return handle(GetAllChatMessagesProcessor.class);
    }

    @POST
    public Response post(String payload) {
        return handle(payload, PostChatMessageProcessor.class);
    }

}
