package de.sollder1.messanger.processors.partner;

import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.business.user.security.impl.SecurityDao;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.messanger.business.modules.messages.MessagesFacade;
import de.sollder1.messanger.business.modules.messages.entities.ChatPartner;
import de.sollder1.messanger.messagemodel.partner.ChatPartnerRequest;
import de.sollder1.messanger.messagemodel.partner.ChatPartnerResponse;
import jakarta.inject.Inject;

public class PostChatPartnerProcessor extends Processor<ChatPartnerRequest, ChatPartnerResponse> {

    @Inject
    private MessagesFacade messagesFacade;
    @Inject
    private ChatPartnerMapper chatPartnerMapper;
    @Inject
    private Security security;
    @Inject
    private SecurityDao securityDao;

    @Override
    public ChatPartnerResponse process(ChatPartnerRequest request) throws Exception {
        ChatPartner partner = new ChatPartner();
        partner.setFirstUser(security.getLoggedInAccount());
        partner.setSecondUser(securityDao.getAccountByUserName(request.getPayload().getOtherUser().getUsername()).orElseThrow());
        return new ChatPartnerResponse(chatPartnerMapper.map(messagesFacade.addPartner(partner)));
    }
}
