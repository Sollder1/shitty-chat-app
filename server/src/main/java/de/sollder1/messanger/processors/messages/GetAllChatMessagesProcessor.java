package de.sollder1.messanger.processors.messages;

import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.messanger.business.modules.messages.MessagesFacade;
import de.sollder1.messanger.messagemodel.messages.ChatMessagesResponse;
import jakarta.inject.Inject;

import java.util.stream.Collectors;

public class GetAllChatMessagesProcessor extends Processor<QueryRequest, ChatMessagesResponse> {

    @Inject
    private MessagesFacade messagesFacade;
    @Inject
    private ChatMessageMapper chatMessageMapper;

    @Override
    public ChatMessagesResponse process(QueryRequest request) throws Exception {
        var messages = messagesFacade
                .getChatMessages(request.getPathParam("chatpartner-id"))
                .stream().map(chatMessageMapper::map)
                .collect(Collectors.toList());
        return new ChatMessagesResponse(messages);
    }


}
