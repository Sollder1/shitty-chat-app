package de.sollder1.messanger.processors.messages;

import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.processing.messagemodel.response.BaseResponseMapper;
import de.sollder1.base.plugins.account.processors.AccountMapper;
import de.sollder1.messanger.business.modules.messages.entities.ChatMessage;
import de.sollder1.messanger.business.modules.messages.entities.ChatPartner;
import de.sollder1.messanger.messagemodel.messages.ChatMessagePayload;
import de.sollder1.messanger.messagemodel.partner.ChatPartnerPayload;
import de.sollder1.messanger.processors.partner.ChatPartnerMapper;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class ChatMessageMapper {

    @Inject
    private ChatPartnerMapper chatPartnerMapper;

    public ChatMessagePayload map(ChatMessage message) {
        ChatMessagePayload payload = new ChatMessagePayload();
        payload.setMessageEncrypted(message.getMessageEncrypted());
        payload.setSender(AccountMapper.mapToPayload(message.getSender()));
       // payload.setChatPartner(chatPartnerMapper.map(message.getChatPartner()));
        BaseResponseMapper.mapToPayload(payload, message);
        return payload;
    }
}
