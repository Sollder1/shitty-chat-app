package de.sollder1.messanger.processors.partner;

import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.messanger.business.modules.messages.MessagesFacade;
import de.sollder1.messanger.messagemodel.partner.ChatPartnersResponse;
import jakarta.inject.Inject;

import java.util.stream.Collectors;

public class GetAllChatPartnerProcessor extends Processor<QueryRequest, ChatPartnersResponse> {

    @Inject
    private MessagesFacade messagesFacade;
    @Inject
    private ChatPartnerMapper chatPartnerMapper;

    @Override
    public ChatPartnersResponse process(QueryRequest request) throws Exception {
        var data = messagesFacade.getAllPartner().stream().map(chatPartnerMapper::map).collect(Collectors.toList());
        return new ChatPartnersResponse(data);
    }
}
