package de.sollder1.messanger.processors.messages;

import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.messanger.business.modules.messages.MessagesFacade;
import de.sollder1.messanger.business.modules.messages.entities.ChatMessage;
import de.sollder1.messanger.messagemodel.messages.ChatMessagePayload;
import de.sollder1.messanger.messagemodel.messages.ChatMessageRequest;
import de.sollder1.messanger.messagemodel.messages.ChatMessageResponse;
import de.sollder1.messanger.messagemodel.messages.ChatMessagesResponse;
import jakarta.inject.Inject;


public class PostChatMessageProcessor extends Processor<ChatMessageRequest, ChatMessageResponse> {

    @Inject
    private MessagesFacade messagesFacade;
    @Inject
    private ChatMessageMapper chatMessageMapper;
    @Inject
    private Security security;

    @Override
    public ChatMessageResponse process(ChatMessageRequest request) throws Exception {
        ChatMessage newMessage = mapAndPersistMessage(request);
        messagesFacade.notifyReceiver(newMessage);
        return new ChatMessageResponse(chatMessageMapper.map(newMessage));
    }

    private ChatMessage mapAndPersistMessage(ChatMessageRequest request) {
        ChatMessage newMessage = new ChatMessage();
        newMessage.setSender(security.getLoggedInAccount());
        newMessage.setMessageEncrypted(request.getPayload().getMessageEncrypted());
        newMessage.setChatPartner(messagesFacade.getPartner(request.getPathParam("chatpartner-id")).orElseThrow());
        newMessage = messagesFacade.persistChatMessage(newMessage);
        return newMessage;
    }
}
