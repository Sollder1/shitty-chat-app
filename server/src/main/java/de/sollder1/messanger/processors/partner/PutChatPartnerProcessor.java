package de.sollder1.messanger.processors.partner;

import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.messanger.business.modules.messages.MessagesFacade;
import de.sollder1.messanger.business.modules.messages.entities.ChatPartner;
import de.sollder1.messanger.messagemodel.partner.ChatPartnerPayload;
import de.sollder1.messanger.messagemodel.partner.ChatPartnerRequest;
import de.sollder1.messanger.messagemodel.partner.ChatPartnerResponse;
import jakarta.inject.Inject;

public class PutChatPartnerProcessor extends Processor<ChatPartnerRequest, ChatPartnerResponse> {

    @Inject
    private MessagesFacade messagesFacade;
    @Inject
    private ChatPartnerMapper chatPartnerMapper;
    @Inject
    private AccountFacade accountFacade;
    @Inject
    private Security security;

    @Override
    public ChatPartnerResponse process(ChatPartnerRequest request) throws Exception {
        ChatPartner partner = messagesFacade.getPartner(request.getPayload().getId()).orElseThrow();
        this.mapPut(partner, request.getPayload());
        partner = messagesFacade.handleStateChange(partner);
        return new ChatPartnerResponse(chatPartnerMapper.map(partner));
    }

    private void mapPut(ChatPartner partner, ChatPartnerPayload payload) {
        partner.setStatus(ChatPartner.Status.valueOf(payload.getStatus()));
        partner.setPublicRsaKey(payload.getPublicRsaKey());
        partner.setSymmetricKeyEncrypted(payload.getSymmetricKeyEncrypted());
    }
}
