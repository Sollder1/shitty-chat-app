package de.sollder1.messanger.processors.partner;

import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.processing.messagemodel.response.BaseResponseMapper;
import de.sollder1.base.plugins.account.processors.AccountMapper;
import de.sollder1.messanger.business.modules.messages.entities.ChatPartner;
import de.sollder1.messanger.messagemodel.partner.ChatPartnerPayload;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class ChatPartnerMapper {

    @Inject
    private Security security;

    public ChatPartnerPayload map(ChatPartner chatPartner) {
        ChatPartnerPayload payload = new ChatPartnerPayload();

        if (security.getLoggedInAccount().equals(chatPartner.getFirstUser())) {
            payload.setOtherUser(AccountMapper.mapToPayload(chatPartner.getSecondUser()));
            payload.setInitiator(true);
        } else {
            payload.setOtherUser(AccountMapper.mapToPayload(chatPartner.getFirstUser()));
            payload.setInitiator(false);
        }

        payload.setStatus(chatPartner.getStatus().toString());
        payload.setPublicRsaKey(chatPartner.getPublicRsaKey());
        payload.setSymmetricKeyEncrypted(chatPartner.getSymmetricKeyEncrypted());
        BaseResponseMapper.mapToPayload(payload, chatPartner);
        return payload;
    }
}
