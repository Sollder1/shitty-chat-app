package de.sollder1.messanger.business.modules.notifications;

import de.sollder1.base.core.databinding.impl.databinder.json.JsonMapperPool;
import de.sollder1.messanger.business.modules.messages.entities.ChatMessage;
import jakarta.inject.Singleton;
import jakarta.websocket.Session;

@Singleton
public class NotificationFacade {

    public void sendNewMessageSendNotifications(ChatMessage newMessage) {
        WebsocketSessionHolder.getSession(newMessage.getReceiver().getId())
                .map(WebsocketSessionHolder.SessionPojo::getOpenSessions)
                .ifPresent(sessions -> sessions.forEach(session -> sendNewMessageSendNotification(newMessage, session)));
    }

    private void sendNewMessageSendNotification(ChatMessage newMessage, Session session) {
        UpdateMessage message = new UpdateMessage();
        message.setMessageType(UpdateMessage.MessageType.NEW_MESSAGE);
        message.setPartnerId(newMessage.getChatPartner().getId());
        message.setNewMessage(newMessage.getMessageEncrypted());
        try {
            var json = JsonMapperPool.getMapper().writeValueAsString(message);
            session.getAsyncRemote().sendText(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
