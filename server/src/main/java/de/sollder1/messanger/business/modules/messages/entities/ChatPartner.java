package de.sollder1.messanger.business.modules.messages.entities;

import de.sollder1.base.core.business.persitence.db.entities.BaseEntity;
import de.sollder1.base.core.business.user.entities.Account;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Entity(name = "chat_partners")
public class ChatPartner extends BaseEntity {

    @JoinColumn(name = "user1_account_id")
    @ManyToOne
    //Hint: Always initiator...
    private Account firstUser;
    @JoinColumn(name = "user2_account_id")
    @ManyToOne
    private Account secondUser;
    @Enumerated(EnumType.STRING)
    private Status status;
    @Column(name = "public_rsa_key")
    private String publicRsaKey;
    @Column(name = "symmetric_key_encrypted")
    private byte[] symmetricKeyEncrypted;

    /*
    @OneToMany(mappedBy = "chatPartner")
    private Set<ChatMessage> chatMessages;
    */
    public enum Status {
        REQUESTED, RSA_KEY_AVAILABLE, SYMMETRIC_KEY_AVAILABLE, ACTIVE
    }
}