package de.sollder1.messanger.business.modules.messages;

import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.business.util.ListUtil;
import de.sollder1.messanger.business.modules.messages.entities.ChatMessage;
import de.sollder1.messanger.business.modules.messages.entities.ChatMessage_;
import de.sollder1.messanger.business.modules.messages.entities.ChatPartner;
import de.sollder1.messanger.business.modules.messages.entities.ChatPartner_;
import de.sollder1.messanger.business.modules.notifications.NotificationFacade;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.transaction.Transactional;
import liquibase.pro.packaged.M;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Singleton
public class MessagesFacade {

    @Inject
    private JpaManager jpa;
    @Inject
    private Security security;
    @Inject
    private NotificationFacade notificationFacade;
    @Inject
    private StateChangeHandler stateChangeHandler;

    public Collection<ChatPartner> getAllPartner() {
        return jpa.buildTypeQuery((cb, root, cq) -> {
            return ListUtil.get(hasAccessPredicate(cb, root));
        }, ChatPartner.class).getResultList();
    }

    public Optional<ChatPartner> getPartner(String chatPartnerId) {
        return jpa.buildTypeQuery((cb, root, cq) -> {
            return ListUtil.get(cb.and(
                    hasAccessPredicate(cb, root),
                    cb.equal(root.get(ChatPartner_.id), chatPartnerId)
            ));
        }, ChatPartner.class).getOptionalSingleResult();
    }

    @Transactional
    public ChatPartner addPartner(ChatPartner newEntity) {
        newEntity.setStatus(ChatPartner.Status.REQUESTED);
        return jpa.merge(newEntity);
    }

    public List<ChatMessage> getChatMessages(String chatPartnerId) {
        var partner = getPartner(chatPartnerId).orElseThrow();
        return jpa.buildTypeQuery((cb, root, cq) -> {
            cq.orderBy(cb.asc(root.get(ChatMessage_.createdAt)));
            return ListUtil.get(cb.equal(root.get(ChatMessage_.chatPartner), partner));
        }, ChatMessage.class).getResultList();
    }

    @Transactional
    public ChatMessage persistChatMessage(ChatMessage message) {
        if(message.getChatPartner().getStatus() != ChatPartner.Status.ACTIVE) {
            throw new RuntimeException("Status muss ACTIVE sein!") ;
        }
        return jpa.merge(message);
    }

    private Predicate hasAccessPredicate(CriteriaBuilder cb, Path<ChatPartner> root) {
        return cb.or(
                cb.equal(root.get(ChatPartner_.firstUser), security.getLoggedInAccount()),
                cb.equal(root.get(ChatPartner_.secondUser), security.getLoggedInAccount())
        );
    }

    public void notifyReceiver(ChatMessage newMessage) {
        notificationFacade.sendNewMessageSendNotifications(newMessage);
    }
    @Transactional
    public ChatPartner handleStateChange(ChatPartner partner) {
        return stateChangeHandler.handle(getPartner(partner.getId()).orElseThrow(), partner);
    }
}
