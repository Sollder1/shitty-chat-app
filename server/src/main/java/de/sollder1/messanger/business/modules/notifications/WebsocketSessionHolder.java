package de.sollder1.messanger.business.modules.notifications;

import de.sollder1.base.core.business.user.entities.Account;
import jakarta.websocket.Session;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class WebsocketSessionHolder {

    private static final ConcurrentMap<String, SessionPojo> SESSIONS = new ConcurrentHashMap<>();

    private WebsocketSessionHolder() {
    }

    public static synchronized void addSession(String accountId, Session session) {
        if (!SESSIONS.containsKey(accountId)) {
            synchronized (WebsocketSessionHolder.class) {
                if (!SESSIONS.containsKey(accountId)) {
                    SESSIONS.put(accountId, new SessionPojo(accountId));
                }
            }
        }
        SESSIONS.get(accountId).addSession(session);
    }

    public static void removeSession(String accountId, Session session) {
        if (SESSIONS.containsKey(accountId)) {
            SESSIONS.get(accountId).removeSession(session);
        }
    }

    public static Optional<SessionPojo> getSession(String receiverAccountId) {

        return Optional.ofNullable(SESSIONS.get(receiverAccountId));
    }


    @Getter
    @Setter
    public static class SessionPojo {

        private final List<Session> openSessions;
        private final String accountId;

        public SessionPojo(String accountId) {
            this.accountId = accountId;
            this.openSessions = Collections.synchronizedList(new ArrayList<>());
        }

        public void addSession(Session session) {
            openSessions.add(session);
        }

        public void removeSession(Session session) {
            openSessions.remove(session);
        }
    }

}
