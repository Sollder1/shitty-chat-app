package de.sollder1.messanger.business.modules.notifications;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateMessage {
    private String partnerId;
    private byte[] newMessage;
    private MessageType messageType;

    public enum MessageType {
        NEW_MESSAGE
    }
}

