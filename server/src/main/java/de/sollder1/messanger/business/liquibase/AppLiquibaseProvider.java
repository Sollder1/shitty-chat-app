package de.sollder1.messanger.business.liquibase;


import de.sollder1.base.core.business.persitence.liquibase.LiquibaseChangelogProvider;

public class AppLiquibaseProvider implements LiquibaseChangelogProvider {
    @Override
    public String getChangeLogPath() {
        return "liquibase/app/changelog.xml";
    }

    @Override
    public int getOrder() {
        return 1;
    }


}
