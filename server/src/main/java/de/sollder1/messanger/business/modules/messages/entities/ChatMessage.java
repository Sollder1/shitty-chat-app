package de.sollder1.messanger.business.modules.messages.entities;

import de.sollder1.base.core.business.persitence.db.entities.BaseEntity;
import de.sollder1.base.core.business.user.entities.Account;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@Entity(name = "chat_messages")
public class ChatMessage extends BaseEntity {

    @JoinColumn(name = "sender_account_id")
    @ManyToOne
    private Account sender;
    @JoinColumn(name = "chat_partner_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ChatPartner chatPartner;
    @Lob
    @Column(name = "message_encrypted")
    private byte[] messageEncrypted;

    public Account getReceiver() {
        if (Objects.equals(chatPartner.getFirstUser(), sender)) {
            return chatPartner.getSecondUser();
        }
        return chatPartner.getFirstUser();
    }
}
