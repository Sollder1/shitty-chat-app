package de.sollder1.messanger.business.modules.notifications;

import de.sollder1.base.core.business.user.exception.InvalidTokenException;
import de.sollder1.base.core.business.user.security.impl.SecurityDao;
import jakarta.inject.Inject;
import jakarta.websocket.*;
import jakarta.websocket.server.PathParam;
import jakarta.websocket.server.ServerEndpoint;
import org.tinylog.Logger;

@ServerEndpoint("/notifications/{token}")
public class WebsocketEndpoint {

    @Inject
    private SecurityDao securityDao;
    private String accountId;

    @OnOpen
    public void onOpen(Session session, @PathParam("token") String token) {
        session.setMaxIdleTimeout(0);
        this.accountId = securityDao.getAuthToken(token).orElseThrow(InvalidTokenException::new).getAccount().getId();
        WebsocketSessionHolder.addSession(accountId, session);
    }

    @OnMessage
    public void onMessage(Session session, String message) {
        Logger.warn("Illegally sent message: {}.", message);
        session.getAsyncRemote().sendText("Received message, this websocket does not accept messages, only sends them.");
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        WebsocketSessionHolder.removeSession(accountId, session);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        Logger.error(throwable, "Something went wrong in the websocket: {}.", session);
        WebsocketSessionHolder.removeSession(accountId, session);
    }

}
