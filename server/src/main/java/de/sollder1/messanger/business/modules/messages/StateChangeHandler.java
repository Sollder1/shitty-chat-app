package de.sollder1.messanger.business.modules.messages;

import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import de.sollder1.messanger.business.modules.messages.entities.ChatPartner;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class StateChangeHandler {

    @Inject
    private JpaManager jpa;

    public ChatPartner handle(ChatPartner before, ChatPartner after) {

        if (before.getStatus() == ChatPartner.Status.REQUESTED && after.getStatus() == ChatPartner.Status.RSA_KEY_AVAILABLE) {
            //TODO: Send websocket maybe...?
        } else if (before.getStatus() == ChatPartner.Status.RSA_KEY_AVAILABLE && after.getStatus() == ChatPartner.Status.SYMMETRIC_KEY_AVAILABLE) {
            after.setPublicRsaKey(null);
        } else if (before.getStatus() == ChatPartner.Status.SYMMETRIC_KEY_AVAILABLE && after.getStatus() == ChatPartner.Status.ACTIVE) {
            after.setSymmetricKeyEncrypted(null);
        } else {
            throw new RuntimeException("Illegal State change!");
        }

        return jpa.merge(after);
    }

}
