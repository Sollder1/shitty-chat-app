package de.sollder1.base.plugins.auth;

import de.sollder1.base.core.business.localization.api.Translator;
import de.sollder1.base.core.business.localization.api.TranslatorBundle;
import de.sollder1.base.core.business.user.exception.AuthenticationException;
import de.sollder1.base.core.processing.exceptionhandling.ExceptionMapper;
import jakarta.inject.Inject;

import java.util.Optional;
import java.util.OptionalInt;

public class AuthExceptionMapper implements ExceptionMapper {

    @Inject
    @TranslatorBundle("auth")
    private Translator translator;

    @Override
    public Optional<String> getError(Exception e) {
        if (e instanceof AuthenticationException) {
            //TODO: translate...!
            return Optional.ofNullable(translator.get(e.getMessage()));
        }
        return Optional.empty();
    }

    @Override
    public OptionalInt getStatusCode(Exception e) {

        if (e instanceof AuthenticationException) {
            return OptionalInt.of(401);
        }

        return OptionalInt.empty();
    }
}
