package de.sollder1.base.plugins.files.messagemodel.getfiles;

import de.sollder1.base.core.processing.messagemodel.defaults.EmptyPayload;
import de.sollder1.base.core.processing.messagemodel.request.RestRequest;


public class GetFileRequest extends RestRequest<EmptyPayload> {
    @Override
    public Class<EmptyPayload> getPayloadClazz() {
        return EmptyPayload.class;
    }
}
