package de.sollder1.base.plugins.account.processors.my;


import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.processing.messagemodel.defaults.EmptyResponse;
import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import jakarta.inject.Inject;

public class DeleteMyAccountProcessor extends Processor<QueryRequest, EmptyResponse> {

    @Inject
    private Security security;
    @Inject
    private AccountFacade accountFacade;

    @Override
    public EmptyResponse process(QueryRequest request) throws Exception {
        accountFacade.deleteAccount(security.getLoggedInAccount().getId());
        return new EmptyResponse();
    }
}
