package de.sollder1.base.plugins.files.processors;


import de.sollder1.base.core.business.files.FileEntity;
import de.sollder1.base.plugins.files.messagemodel.FileMetaPayload;
import de.sollder1.base.core.processing.messagemodel.response.BaseResponseMapper;

public class FileMapper {

    public static FileMetaPayload mapToPayload(FileEntity file) {
        FileMetaPayload payload = new FileMetaPayload();
        payload.setFilename(file.getFilename());
        payload.setSizeInBytes(file.getSizeInBytes());
        BaseResponseMapper.mapToPayload(payload, file);
        return payload;
    }

}
