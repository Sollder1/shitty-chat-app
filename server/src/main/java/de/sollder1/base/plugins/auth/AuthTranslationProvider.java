package de.sollder1.base.plugins.auth;


import de.sollder1.base.core.business.localization.api.TranslationProvider;

import java.util.*;

public class AuthTranslationProvider implements TranslationProvider {


    @Override
    public List<Locale> getSupportedLocales() {
        return Arrays.asList(Locale.GERMAN, Locale.ENGLISH, Locale.FRENCH);
    }

    @Override
    public List<String> getBundleNames() {
        return Collections.singletonList("auth");
    }

    @Override
    public ResourceBundle getResourceBundle(String bundleName, Locale supportedLocale) {
        return ResourceBundle.getBundle("localization." + bundleName, supportedLocale);
    }
}
