package de.sollder1.base.plugins.files;


import de.sollder1.base.core.databinding.impl.databinder.file.FileRequestDataBinder;
import de.sollder1.base.core.connection.BasicEndpoint;
import de.sollder1.base.core.databinding.impl.databinder.file.FileResponseDataBinder;
import de.sollder1.base.plugins.files.processors.GetFileProcessor;
import de.sollder1.base.plugins.files.processors.PostFileSimpleProcessor;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;


@Path("file")
public class FileEndpoint extends BasicEndpoint {

    @POST
    @Consumes({MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_OCTET_STREAM})
    @Path("simple")
    public Response upload(MultipartBody body) {
        var attachment = body.getAttachment("file");
        return handle(attachment, PostFileSimpleProcessor.class, new FileRequestDataBinder());
    }

    @GET
    @Path("{file-id}")
    public Response download() {
        return handle("{}", GetFileProcessor.class, new FileResponseDataBinder());
    }

}
