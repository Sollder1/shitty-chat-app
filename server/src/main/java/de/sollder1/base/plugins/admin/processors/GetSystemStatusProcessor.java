package de.sollder1.base.plugins.admin.processors;


import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.plugins.admin.business.AdminFacade;
import de.sollder1.base.plugins.admin.messagemodel.GetSystemStatusResponse;
import jakarta.inject.Inject;

public class GetSystemStatusProcessor extends Processor<QueryRequest, GetSystemStatusResponse> {

    @Inject
    private AdminFacade adminFacade;

    @Override
    public GetSystemStatusResponse process(QueryRequest request) throws Exception {
        return new GetSystemStatusResponse(adminFacade.getCurrentSystemStatus());
    }

    @Override
    public boolean authenticate() {
        return true;
    }

    @Override
    public boolean mustBeAdmin() {
        return true;
    }
}
