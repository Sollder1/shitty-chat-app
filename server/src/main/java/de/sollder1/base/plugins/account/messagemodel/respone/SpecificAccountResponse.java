package de.sollder1.base.plugins.account.messagemodel.respone;


import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import de.sollder1.base.plugins.account.messagemodel.AccountPayload;

public class SpecificAccountResponse extends RestResponse<AccountPayload> {
    public SpecificAccountResponse() {
    }

    public SpecificAccountResponse(AccountPayload payload) {
        super(payload);
    }
}
