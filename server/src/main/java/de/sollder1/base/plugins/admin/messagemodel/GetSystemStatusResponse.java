package de.sollder1.base.plugins.admin.messagemodel;


import de.sollder1.base.core.processing.messagemodel.response.RestResponse;

import java.util.List;

public class GetSystemStatusResponse extends RestResponse<GetSystemStatusResponsePayload> {
    public GetSystemStatusResponse() {
    }

    public GetSystemStatusResponse(List<String> data) {
        GetSystemStatusResponsePayload payload = new GetSystemStatusResponsePayload();
        payload.setValues(data);
        setStatusCode(200);
        setPayload(payload);
    }
}
