package de.sollder1.base.plugins.account.processors;


import de.sollder1.base.core.business.user.entities.Account;
import de.sollder1.base.core.business.user.entities.Gender;
import de.sollder1.base.core.business.user.role.RoleFacade;
import de.sollder1.base.core.processing.messagemodel.response.BaseResponseMapper;
import de.sollder1.base.plugins.account.messagemodel.AccountPayload;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import java.util.Objects;

@Singleton
public class AccountMapper {

    @Inject
    private RoleFacade roleFacade;


    public static AccountPayload mapToPayload(Account account) {
        var payload = new AccountPayload();
        BaseResponseMapper.mapToPayload(payload, account);
        payload.setUsername(account.getUsername());
        payload.setEmail(account.getAssignedPerson().getEmail());
        payload.setFirstname(account.getAssignedPerson().getFirstname());
        payload.setLastname(account.getAssignedPerson().getLastname());
        payload.setGender(account.getAssignedPerson().getGender().toString());
        payload.setProfilePictureImage(account.getAssignedPerson().getProfilePictureUrl());
        payload.setAdmin(account.isAdmin());
        payload.setBlocked(account.isBlocked());

        if (account.getRole() != null) {
            payload.setRoleId(account.getRole().getId());
        }

        return payload;
    }


    public static Account mapToMyEntity(Account account, AccountPayload payload) {
        account.setUsername(payload.getUsername());
        var person = account.getAssignedPerson();
        person.setEmail(payload.getEmail());
        person.setFirstname(payload.getFirstname());
        person.setLastname(payload.getLastname());
        person.setGender(Gender.valueOf(payload.getGender()));
        return account;
    }


    public Account mapToEntity(Account account, AccountPayload payload) {

        mapToMyEntity(account, payload);
        account.setAdmin(payload.isAdmin());
        account.setBlocked(payload.isBlocked());
        if (payload.getRoleId() != null) {
            account.setRole(roleFacade.getById(payload.getRoleId()).orElseThrow());
        }
        if (Objects.nonNull(payload.getPassword())) {
            account.setChangedPassword(true);
            account.setPassword(payload.getPassword());
        }

        account.setBlockReason("");

        return account;

    }
}
