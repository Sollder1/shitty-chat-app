package de.sollder1.base.plugins.roles.messagemodel.respone;


import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import de.sollder1.base.plugins.roles.messagemodel.RolePayload;

import java.util.List;

public class QueryRolesResponse extends RestResponse<List<RolePayload>> {

    public QueryRolesResponse() {
    }

    public QueryRolesResponse(List<RolePayload> payload) {
        super(payload);
    }
}
