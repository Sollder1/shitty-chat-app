package de.sollder1.base.plugins.admin.messagemodel;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GetSystemStatusResponsePayload {

    private List<String> values;

}
