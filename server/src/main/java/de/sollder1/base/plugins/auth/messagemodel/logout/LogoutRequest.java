package de.sollder1.base.plugins.auth.messagemodel.logout;

import de.sollder1.base.core.processing.messagemodel.defaults.EmptyPayload;
import de.sollder1.base.core.processing.messagemodel.request.RestRequest;

public class LogoutRequest extends RestRequest<EmptyPayload> {

}
