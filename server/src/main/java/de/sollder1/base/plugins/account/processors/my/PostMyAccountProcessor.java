package de.sollder1.base.plugins.account.processors.my;


import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.business.user.entities.Account;
import de.sollder1.base.core.business.user.entities.Person;
import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.business.user.security.impl.PasswordUtils;
import de.sollder1.base.core.processing.messagemodel.defaults.EmptyResponse;
import de.sollder1.base.plugins.account.messagemodel.AccountPayload;
import de.sollder1.base.plugins.account.messagemodel.request.SaveAccountRequest;
import de.sollder1.base.plugins.account.processors.AccountMapper;
import de.sollder1.base.plugins.auth.messagemodel.login.LoginResponse;
import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.plugins.auth.processors.AuthMapper;
import jakarta.inject.Inject;

public class PostMyAccountProcessor extends Processor<SaveAccountRequest, EmptyResponse> {

    @Inject
    private AccountFacade accountFacade;

    @Override
    public EmptyResponse process(SaveAccountRequest request) throws Exception {
        accountFacade.registerAccount(mapAccountRegistrationData(request.getPayload()));
        return new EmptyResponse();
    }

    private Account mapAccountRegistrationData(AccountPayload payload) {
        Account account = new Account();
        account.setUsername(payload.getUsername());
        account.setPassword(payload.getPassword());
        return account;
    }

    @Override
    public boolean authenticate() {
        return false;
    }
}
