package de.sollder1.base.plugins.account.processors.admin;


import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.plugins.account.messagemodel.respone.QueryAccountResponse;
import de.sollder1.base.plugins.account.processors.AccountMapper;
import jakarta.inject.Inject;

import java.util.stream.Collectors;

public class GetAccountsProcessor extends Processor<QueryRequest, QueryAccountResponse> {

    @Inject
    private AccountFacade accountFacade;

    @Override
    public QueryAccountResponse process(QueryRequest request) throws Exception {
        var account = accountFacade.queryAccounts(request.getQueryParam("query"));
        var payload = account.stream().map(AccountMapper::mapToPayload).collect(Collectors.toList());
        return new QueryAccountResponse(payload);
    }

    @Override
    public boolean mustBeAdmin() {
        return true;
    }

}
