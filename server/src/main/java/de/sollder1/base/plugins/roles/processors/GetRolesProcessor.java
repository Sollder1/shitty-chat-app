package de.sollder1.base.plugins.roles.processors;


import de.sollder1.base.core.business.user.role.RoleFacade;
import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.plugins.roles.messagemodel.respone.QueryRolesResponse;
import jakarta.inject.Inject;

import java.util.stream.Collectors;

public class GetRolesProcessor extends Processor<QueryRequest, QueryRolesResponse> {

    @Inject
    private RoleFacade roleFacade;

    @Override
    public QueryRolesResponse process(QueryRequest request) throws Exception {
        var roles = roleFacade.getAll().stream()
                .map(RolesMapper::mapToRolePayload).collect(Collectors.toList());
        return new QueryRolesResponse(roles);
    }
}
