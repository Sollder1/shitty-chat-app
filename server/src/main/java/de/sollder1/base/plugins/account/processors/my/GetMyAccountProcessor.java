package de.sollder1.base.plugins.account.processors.my;


import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.plugins.account.messagemodel.respone.SpecificAccountResponse;
import de.sollder1.base.plugins.account.processors.AccountMapper;
import jakarta.inject.Inject;

public class GetMyAccountProcessor extends Processor<QueryRequest, SpecificAccountResponse> {

    @Inject
    private Security security;

    @Override
    public SpecificAccountResponse process(QueryRequest request) throws Exception {
        var payload = AccountMapper.mapToPayload(security.getLoggedInAccount());
        return new SpecificAccountResponse(payload);
    }
}
