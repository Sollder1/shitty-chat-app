package de.sollder1.base.plugins.account.processors.admin;


import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.plugins.account.messagemodel.respone.SpecificAccountResponse;
import de.sollder1.base.plugins.account.processors.AccountMapper;
import jakarta.inject.Inject;

public class GetAccountProcessor extends Processor<QueryRequest, SpecificAccountResponse> {

    @Inject
    private AccountFacade accountFacade;

    @Override
    public SpecificAccountResponse process(QueryRequest request) throws Exception {
        var account = accountFacade.getAccount(request.getPathParam("account-id"));
        var payload = AccountMapper.mapToPayload(account.orElseThrow());
        return new SpecificAccountResponse(payload);
    }

    @Override
    public boolean mustBeAdmin() {
        return true;
    }

}
