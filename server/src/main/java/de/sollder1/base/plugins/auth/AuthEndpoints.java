package de.sollder1.base.plugins.auth;

import de.sollder1.base.core.connection.BasicEndpoint;
import de.sollder1.base.plugins.auth.processors.LoginProcessor;
import de.sollder1.base.plugins.auth.processors.LogoutProcessor;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("auth")
public class AuthEndpoints extends BasicEndpoint {

    @POST
    public Response login(String payload) {
        return handle(payload, LoginProcessor.class);
    }

    @DELETE
    public Response logout() {
        return handle("{}", LogoutProcessor.class);
    }

}
