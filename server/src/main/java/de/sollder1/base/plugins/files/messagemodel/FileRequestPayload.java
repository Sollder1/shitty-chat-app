package de.sollder1.base.plugins.files.messagemodel;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

public class FileRequestPayload {

    private Attachment file;

    public FileRequestPayload(Attachment file) {
        this.file = file;
    }

    public Attachment getFile() {
        return file;
    }

    public void setFile(Attachment file) {
        this.file = file;
    }
}
