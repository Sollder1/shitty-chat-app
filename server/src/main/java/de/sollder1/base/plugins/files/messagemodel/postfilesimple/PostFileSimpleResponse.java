package de.sollder1.base.plugins.files.messagemodel.postfilesimple;

import de.sollder1.base.plugins.files.messagemodel.FileMetaPayload;
import de.sollder1.base.core.processing.messagemodel.response.RestResponse;

public class PostFileSimpleResponse extends RestResponse<FileMetaPayload> {
    public PostFileSimpleResponse() {
        super();
    }

    public PostFileSimpleResponse(FileMetaPayload payload) {
        super(payload);
    }
}
