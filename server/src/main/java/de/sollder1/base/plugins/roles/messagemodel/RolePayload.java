package de.sollder1.base.plugins.roles.messagemodel;


import de.sollder1.base.core.processing.messagemodel.response.BaseResponsePayload;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RolePayload extends BaseResponsePayload {

    private String name;
    private List<String> permissions;

}
