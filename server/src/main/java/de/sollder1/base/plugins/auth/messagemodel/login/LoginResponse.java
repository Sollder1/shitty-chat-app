package de.sollder1.base.plugins.auth.messagemodel.login;

import de.sollder1.base.core.processing.messagemodel.response.RestResponse;

public class LoginResponse extends RestResponse<LoginResponsePayload> {

    public LoginResponse() {
    }

    public LoginResponse(LoginResponsePayload payload) {
        super(payload);
    }
}
