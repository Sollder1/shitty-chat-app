package de.sollder1.base.plugins.account.processors.admin;


import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.processing.messagemodel.defaults.EmptyResponse;
import de.sollder1.base.core.processing.messagemodel.defaults.QueryRequest;
import de.sollder1.base.core.processing.processors.Processor;
import jakarta.inject.Inject;

public class DeleteAccountProcessor extends Processor<QueryRequest, EmptyResponse> {
    
    @Inject
    private AccountFacade accountFacade;

    @Override
    public EmptyResponse process(QueryRequest request) throws Exception {
        accountFacade.deleteAccount(request.getPathParam("account-id"));
        return new EmptyResponse();
    }

    @Override
    public boolean mustBeAdmin() {
        return true;
    }
}
