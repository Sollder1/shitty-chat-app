package de.sollder1.base.plugins.roles.messagemodel;


import de.sollder1.base.core.processing.messagemodel.response.BaseResponsePayload;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PermissionPayload extends BaseResponsePayload {
    private String name;
}
