package de.sollder1.base.plugins.account.processors.my;


import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.plugins.account.messagemodel.request.SaveAccountRequest;
import de.sollder1.base.plugins.account.messagemodel.respone.SaveAccountResponse;
import de.sollder1.base.plugins.account.processors.AccountMapper;
import jakarta.inject.Inject;

public class PutMyAccountProcessor extends Processor<SaveAccountRequest, SaveAccountResponse> {

    @Inject
    private Security security;
    @Inject
    private AccountFacade accountFacade;

    @Override
    public SaveAccountResponse process(SaveAccountRequest request) throws Exception {
        var entity = AccountMapper.mapToMyEntity(security.getLoggedInAccount(), request.getPayload());
        entity = accountFacade.updateAccount(entity);
        return new SaveAccountResponse(AccountMapper.mapToPayload(entity));
    }
}
