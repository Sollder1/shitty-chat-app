package de.sollder1.base.plugins.auth.processors;

import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.plugins.auth.messagemodel.logout.LogoutRequest;
import de.sollder1.base.plugins.auth.messagemodel.logout.LogoutResponse;
import de.sollder1.base.core.processing.messagemodel.defaults.EmptyPayload;
import de.sollder1.base.core.processing.processors.Processor;
import jakarta.inject.Inject;

public class LogoutProcessor extends Processor<LogoutRequest, LogoutResponse> {

    @Inject
    private Security security;

    @Override
    public LogoutResponse process(LogoutRequest request) throws Exception {
        security.logout(true);
        return new LogoutResponse(new EmptyPayload());
    }
}
