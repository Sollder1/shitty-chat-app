package de.sollder1.base.plugins.account.messagemodel;


import de.sollder1.base.core.processing.messagemodel.response.BaseResponsePayload;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountPayload extends BaseResponsePayload {

    private String firstname;
    private String lastname;
    private String gender;
    private String email;
    private String username;
    private String password;
    private String profilePictureImage;
    private boolean admin;
    private String roleId;
    private boolean blocked;
    
}
