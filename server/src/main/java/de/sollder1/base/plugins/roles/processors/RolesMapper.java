package de.sollder1.base.plugins.roles.processors;

import de.sollder1.base.core.business.user.entities.PermissionEntity;
import de.sollder1.base.core.business.user.entities.RoleEntity;
import de.sollder1.base.plugins.roles.messagemodel.RolePayload;

import java.util.stream.Collectors;

public class RolesMapper {

    public static RolePayload mapToRolePayload(RoleEntity role) {

        var payload = new RolePayload();
        payload.setId(role.getId());
        payload.setName(role.getName());

        return payload;
    }

    private static RolePayload mapToRolePayloadWithPermissions(RoleEntity role) {
        var payload = mapToRolePayload(role);
        payload.setPermissions(role.getPermissions().stream()
                .map(PermissionEntity::getName).collect(Collectors.toList()));
        return payload;
    }

}
