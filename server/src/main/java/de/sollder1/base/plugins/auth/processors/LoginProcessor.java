package de.sollder1.base.plugins.auth.processors;


import de.sollder1.base.core.business.user.entities.AuthToken;
import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.plugins.auth.messagemodel.login.LoginRequest;
import de.sollder1.base.plugins.auth.messagemodel.login.LoginResponse;
import de.sollder1.base.core.processing.processors.Processor;
import jakarta.inject.Inject;

public class LoginProcessor extends Processor<LoginRequest, LoginResponse> {

    @Inject
    private Security security;

    @Override
    public LoginResponse process(LoginRequest request) {

        AuthToken token;
        if (request.getPayload().getGoogleAuthToken() != null) {
            token = security.loginWithGoogle(request.getPayload().getGoogleAuthToken());
        } else {
            token = security.login(request.getPayload().getUsername(), request.getPayload().getPassword());
        }
        var payload = AuthMapper.mapToLoginPayload(security.getLoggedInAccount(), token);
        return new LoginResponse(payload);
    }

    @Override
    public boolean authenticate() {
        return false;
    }
}
