package de.sollder1.base.plugins.files.processors;

import de.sollder1.base.plugins.files.messagemodel.FileResponsePayload;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.core.business.files.FilesFacade;
import de.sollder1.base.plugins.files.messagemodel.getfiles.GetFileRequest;
import de.sollder1.base.plugins.files.messagemodel.getfiles.GetFileResponse;
import jakarta.inject.Inject;

public class GetFileProcessor extends Processor<GetFileRequest, GetFileResponse> {

    @Inject
    private FilesFacade filesFacade;

    @Override
    public GetFileResponse process(GetFileRequest request) throws Exception {

        var entity = filesFacade.getEntityById(request.getPathParam("file-id"));
        var result = filesFacade.readFileFromStore(entity);
        var payload = new FileResponsePayload();
        payload.setFile(result);
        payload.setFileName(entity.getFilename());

        return new GetFileResponse(payload);
    }

    @Override
    public boolean authenticate() {
        return false;
    }
}
