package de.sollder1.base.plugins.files.messagemodel;


import de.sollder1.base.core.processing.messagemodel.response.BaseResponsePayload;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileMetaPayload extends BaseResponsePayload {

    private String filename;
    private String url;
    private long sizeInBytes;
    
}
