package de.sollder1.base.plugins.roles.messagemodel.respone;


import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import de.sollder1.base.plugins.roles.messagemodel.RolePayload;

public class SpecificRoleResponse extends RestResponse<RolePayload> {
    public SpecificRoleResponse() {
    }

    public SpecificRoleResponse(RolePayload payload) {
        super(payload);
    }
}
