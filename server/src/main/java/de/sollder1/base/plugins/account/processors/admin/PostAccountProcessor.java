package de.sollder1.base.plugins.account.processors.admin;


import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.business.user.entities.Account;
import de.sollder1.base.core.business.user.entities.Person;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.plugins.account.messagemodel.request.SaveAccountRequest;
import de.sollder1.base.plugins.account.messagemodel.respone.SaveAccountResponse;
import de.sollder1.base.plugins.account.processors.AccountMapper;
import jakarta.inject.Inject;

public class PostAccountProcessor extends Processor<SaveAccountRequest, SaveAccountResponse> {

    @Inject
    private AccountFacade accountFacade;
    @Inject
    private AccountMapper accountMapper;

    @Override
    public SaveAccountResponse process(SaveAccountRequest request) throws Exception {
        var entity = accountMapper.mapToEntity(new Account(new Person()), request.getPayload());
        entity = accountFacade.updateAccount(entity);
        return new SaveAccountResponse(AccountMapper.mapToPayload(entity));
    }

    @Override
    public boolean mustBeAdmin() {
        return true;
    }
}
