package de.sollder1.base.plugins.account.connection;


import de.sollder1.base.core.connection.BasicEndpoint;
import de.sollder1.base.plugins.account.processors.my.DeleteMyAccountProcessor;
import de.sollder1.base.plugins.account.processors.my.GetMyAccountProcessor;
import de.sollder1.base.plugins.account.processors.my.PostMyAccountProcessor;
import de.sollder1.base.plugins.account.processors.my.PutMyAccountProcessor;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

@Path("accounts/me")
public class MyAccountEndpoint extends BasicEndpoint {

    @DELETE
    public Response deleteMe() {
        return handle(DeleteMyAccountProcessor.class);
    }

    @GET
    public Response getMe() {
        return handle(GetMyAccountProcessor.class);
    }

    @PUT
    public Response putMe(String payload) {
        return handle(payload, PutMyAccountProcessor.class);
    }

    @POST
    public Response registerMe(String payload) {
        return handle(payload, PostMyAccountProcessor.class);
    }


}
