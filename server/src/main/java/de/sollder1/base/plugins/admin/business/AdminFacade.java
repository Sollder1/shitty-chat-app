package de.sollder1.base.plugins.admin.business;

import jakarta.inject.Singleton;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.tinylog.Logger;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class AdminFacade {

    private static final double MEM_FACTOR = 1024d * 1024d;

    public List<String> getCurrentSystemStatus() {
        List<String> data = new ArrayList<>();

        //OS infos:

        data.add("os.name: " + System.getProperty("os.name"));
        data.add("os.arch: " + System.getProperty("os.arch"));
        data.add("os.version: " + System.getProperty("os.version"));

        //Some external Data...
        data.add("free:\n" + getDataFromCommand("free -h"));
        String pid = getDataFromCommand("cat /opt/tomee/tomcat.pid");
        data.add("tomcatPid: " + pid);
        data.add("Tomee Threads (OS): " + getDataFromCommand(String.format("ps -p %s -lfT | wc -l", pid)));
        data.add("Threads in ThreadGroup: " + Thread.activeCount());

        //Jvm Memory Stuff...
        data.add("totalJvmMemory: " + Runtime.getRuntime().totalMemory() / MEM_FACTOR);
        data.add("usedJvmMemory: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / MEM_FACTOR);
        data.add("freeJvmMemory: " + Runtime.getRuntime().freeMemory() / MEM_FACTOR);
        data.add("maxJvmMemory: " + Runtime.getRuntime().maxMemory() / MEM_FACTOR);


        return data;
    }


    private String getDataFromCommand(String command) {

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            CommandLine cmdLine = CommandLine.parse(command);
            DefaultExecutor executor = new DefaultExecutor();
            PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
            executor.setStreamHandler(streamHandler);
            int exitValue = executor.execute(cmdLine);

            if (exitValue != 0) {
                throw new RuntimeException(String.format("Failed with %d", exitValue));
            }

            return outputStream.toString();
        } catch (Exception e) {
            Logger.error(e, "Bash call failed");
            return "issue";
        }

    }

}
