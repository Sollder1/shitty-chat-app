package de.sollder1.base.plugins.files.processors;

import de.sollder1.base.plugins.files.messagemodel.postfilesimple.PostFileSimpleResponse;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.core.business.files.FilesFacade;
import de.sollder1.base.plugins.files.messagemodel.postfilesimple.PostFileSimpleRequest;
import jakarta.inject.Inject;

import java.io.InputStream;

public class PostFileSimpleProcessor extends Processor<PostFileSimpleRequest, PostFileSimpleResponse> {

    @Inject
    private FilesFacade filesFacade;

    @Override
    public PostFileSimpleResponse process(PostFileSimpleRequest request) throws Exception {

        var payload = request.getPayload().getFile();

        var entity = filesFacade.writeFileToStore(payload.getObject(InputStream.class),
                payload.getContentDisposition().getParameter("filename"));


        return new PostFileSimpleResponse(FileMapper.mapToPayload(entity));
    }

    @Override
    public Class<PostFileSimpleRequest> getRequestType() {
        return PostFileSimpleRequest.class;
    }

    @Override
    public Class<PostFileSimpleResponse> getResponseType() {
        return PostFileSimpleResponse.class;
    }

}
