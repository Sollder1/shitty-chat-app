package de.sollder1.base.plugins.account.messagemodel.respone;


import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import de.sollder1.base.plugins.account.messagemodel.AccountPayload;

import java.util.List;

public class QueryAccountResponse extends RestResponse<List<AccountPayload>> {

    public QueryAccountResponse() {
    }

    public QueryAccountResponse(List<AccountPayload> payload) {
        super(payload);
    }
}
