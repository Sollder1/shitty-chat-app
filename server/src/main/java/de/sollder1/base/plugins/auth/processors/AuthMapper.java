package de.sollder1.base.plugins.auth.processors;

import de.sollder1.base.core.business.user.entities.AuthToken;
import de.sollder1.base.core.business.util.DateTimeUtil;
import de.sollder1.base.plugins.auth.messagemodel.login.LoginResponsePayload;
import de.sollder1.base.core.processing.messagemodel.response.BaseResponseMapper;
import de.sollder1.base.core.business.user.entities.Account;


public class AuthMapper {

    public static LoginResponsePayload mapToLoginPayload(Account account, AuthToken token) {
        LoginResponsePayload payload = new LoginResponsePayload();
        payload.setEmail(account.getAssignedPerson().getEmail());
        payload.setFirstname(account.getAssignedPerson().getFirstname());
        payload.setLastname(account.getAssignedPerson().getLastname());
        payload.setGender(account.getAssignedPerson().getGender().name());
        payload.setUsername(account.getUsername());
        payload.setExpirationDate(DateTimeUtil.to(token.getExpirationDate()));
        payload.setToken(token.getToken());
        payload.setAdmin(account.isAdmin());
        BaseResponseMapper.mapToPayload(payload, token);
        payload.setId(account.getId());

        return payload;

    }

}
