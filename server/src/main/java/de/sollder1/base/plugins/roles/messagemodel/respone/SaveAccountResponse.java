package de.sollder1.base.plugins.roles.messagemodel.respone;


import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import de.sollder1.base.plugins.roles.messagemodel.RolePayload;

public class SaveAccountResponse extends RestResponse<RolePayload> {

    public SaveAccountResponse() {
    }

    public SaveAccountResponse(RolePayload payload) {
        super(payload);
    }
}
