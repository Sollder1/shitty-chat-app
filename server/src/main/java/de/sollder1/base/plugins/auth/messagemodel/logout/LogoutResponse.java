package de.sollder1.base.plugins.auth.messagemodel.logout;

import de.sollder1.base.core.processing.messagemodel.defaults.EmptyPayload;
import de.sollder1.base.core.processing.messagemodel.response.RestResponse;

public class LogoutResponse extends RestResponse<EmptyPayload> {

    public LogoutResponse() {
    }

    public LogoutResponse(EmptyPayload payload) {
        super(payload);
    }
}
