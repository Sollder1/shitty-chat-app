package de.sollder1.base.plugins.account.messagemodel.respone;


import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import de.sollder1.base.plugins.account.messagemodel.AccountPayload;

public class SaveAccountResponse extends RestResponse<AccountPayload> {

    public SaveAccountResponse() {
    }

    public SaveAccountResponse(AccountPayload payload) {
        super(payload);
    }
}
