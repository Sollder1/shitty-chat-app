package de.sollder1.base.plugins.admin.connection;


import de.sollder1.base.core.connection.BasicEndpoint;
import de.sollder1.base.plugins.admin.processors.GetSystemStatusProcessor;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("administration")
public class AdminEndpoint extends BasicEndpoint {

    @GET
    @Path("system-status")
    public Response get() {
        return handle(GetSystemStatusProcessor.class);
    }


}
