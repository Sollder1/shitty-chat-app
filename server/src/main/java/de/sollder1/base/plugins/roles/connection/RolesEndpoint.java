package de.sollder1.base.plugins.roles.connection;


import de.sollder1.base.core.connection.BasicEndpoint;
import de.sollder1.base.plugins.roles.processors.GetRolesProcessor;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("roles")
public class RolesEndpoint extends BasicEndpoint {

    @GET
    public Response getAll() {
        return handle(GetRolesProcessor.class);
    }

}
