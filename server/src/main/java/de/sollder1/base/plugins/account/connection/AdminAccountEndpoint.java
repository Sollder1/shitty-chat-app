package de.sollder1.base.plugins.account.connection;


import de.sollder1.base.core.connection.BasicEndpoint;
import de.sollder1.base.plugins.account.processors.admin.*;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

@Path("accounts/admin")
public class AdminAccountEndpoint extends BasicEndpoint {

    @GET
    public Response getAll() {
        return handle(GetAccountsProcessor.class);
    }

    @POST
    public Response post(String payload) {
        return handle(payload, PostAccountProcessor.class);
    }

    @GET
    @Path("{account-id}")
    public Response get() {
        return handle(GetAccountProcessor.class);
    }

    @PUT
    @Path("{account-id}")
    public Response put(String payload) {
        return handle(payload, PutAccountProcessor.class);
    }

    @DELETE
    @Path("{account-id}")
    public Response delete() {
        return handle(DeleteAccountProcessor.class);
    }
}
