package de.sollder1.base.plugins.files.messagemodel.getfiles;

import de.sollder1.base.plugins.files.messagemodel.FileResponsePayload;
import de.sollder1.base.core.processing.messagemodel.response.RestResponse;

public class GetFileResponse extends RestResponse<FileResponsePayload> {

    public GetFileResponse() {
    }

    public GetFileResponse(FileResponsePayload payload) {
        super(payload);
    }
}
