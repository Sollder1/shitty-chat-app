package de.sollder1.base.core.business.localization.api;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public interface TranslationProvider {

    List<Locale> getSupportedLocales();

    List<String> getBundleNames();

    ResourceBundle getResourceBundle(String bundleName, Locale supportedLocale) throws NoSuchResourceBundleException;


}
