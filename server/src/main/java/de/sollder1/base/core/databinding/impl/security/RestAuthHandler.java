package de.sollder1.base.core.databinding.impl.security;

import de.sollder1.base.core.business.user.exception.AuthenticationException;
import de.sollder1.base.core.business.user.exception.NotLoggedInException;
import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.databinding.impl.exceptions.DatabindingAuthException;
import de.sollder1.base.core.processing.messagemodel.request.RestRequest;
import jakarta.inject.Inject;

import java.util.Optional;

//TODO: Translate stuff...
public class RestAuthHandler {

    @Inject
    private Security security;

    public void authenticate(RestRequest request) throws DatabindingAuthException {
        Optional<String> optionalHeaderField = request.getHeaderOptional("Authorization");

        if (optionalHeaderField.isPresent()) {
            String token = optionalHeaderField.get();
            try {
                security.validateAuthToken(token);
            } catch (AuthenticationException e) {
                e.printStackTrace();
                throw new DatabindingAuthException(401, e.getMessage());
            }
        } else {
            throw new DatabindingAuthException(401, "NoAuthorizationHeaderProvided");
        }

    }

    public void authorize(String permission) throws DatabindingAuthException {

        try {
            boolean isPermitted = security.hasPermission(permission);
            if (!isPermitted) {
                throw new DatabindingAuthException(403, "NotPermittedException");
            }

        } catch (NotLoggedInException e) {
            throw new DatabindingAuthException(401, "NotLoggedIn");
        }
    }

    public void isAdmin() throws DatabindingAuthException {

        try {
            boolean isAdmin = security.isAdmin();
            if (!isAdmin) {
                throw new DatabindingAuthException(403, "NotPermittedException");
            }

        } catch (NotLoggedInException e) {
            throw new DatabindingAuthException(401, "NotLoggedIn");
        }
    }

}
