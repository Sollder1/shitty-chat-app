package de.sollder1.base.core.processing.processors;


import de.sollder1.base.core.processing.exceptionhandling.ExceptionManager;
import de.sollder1.base.core.processing.messagemodel.request.RestRequest;
import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import org.tinylog.Logger;

import java.lang.reflect.ParameterizedType;
import java.util.Optional;

public abstract class Processor<REQUEST extends RestRequest, RESPONSE extends RestResponse> {

    public abstract RESPONSE process(REQUEST request) throws Exception;

    public RestResponse processAndCatch(REQUEST request, ExceptionManager exceptionManager) {

        try {
            return process(request);
        } catch (Exception e) {
            Logger.error(e, "Error during processing of request.");
            return exceptionManager.map(e);
        }
    }

    public Class<REQUEST> getRequestType() {
        return (Class<REQUEST>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public Class<RESPONSE> getResponseType() {
        return (Class<RESPONSE>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }


    public boolean authenticate() {
        return true;
    }

    public boolean mustBeAdmin() {
        return false;
    }

    public Optional<String> authorize() {
        return Optional.empty();
    }


    //TODO: validate..?

}
