package de.sollder1.base.core.databinding.impl.databinder.json;

public class ErrorPayload {

    private final String error;

    public ErrorPayload(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
