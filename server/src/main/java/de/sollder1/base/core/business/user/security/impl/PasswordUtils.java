package de.sollder1.base.core.business.user.security.impl;

import de.sollder1.base.core.business.user.entities.Account;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha512Hash;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class PasswordUtils {

    public static void setPasswordAndSalt(Account account) {
        account.setSalt(getRandomSalt());
        account.setPassword(hashAndSaltPassword(account.getPassword(), account.getSalt()));
    }

    public static boolean passwordsMatch(String hashedPasswordDb, String saltHex, String clearTextPassword) {
        String hashedPassword = hashAndSaltPassword(clearTextPassword, saltHex);
        return MessageDigest.isEqual(hashedPassword.getBytes(StandardCharsets.UTF_8), hashedPasswordDb.getBytes(StandardCharsets.UTF_8));
    }

    public static String hashAndSaltPassword(String clearTextPassword, String salt) {
        return new Sha512Hash(clearTextPassword, salt, 10).toHex();
    }

    public static String getRandomSalt() {
        return new SecureRandomNumberGenerator().nextBytes().toHex();
    }


}
