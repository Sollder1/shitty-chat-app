package de.sollder1.base.core.business.persitence.liquibase;

import jakarta.annotation.Resource;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.stream.Collectors;

@ApplicationScoped
public class LiquibaseManager {

    @Inject
    private Instance<LiquibaseChangelogProvider> providerImplementations;
    @Resource(name = "messangerNJTA")
    private DataSource source;

    public void startUpdate() throws LiquibaseException, SQLException {
        Connection connection = null;
        Database database = null;

        try {
            connection = source.getConnection();
            database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));

            var providers = providerImplementations.stream()
                    .sorted(Comparator.comparingInt(LiquibaseChangelogProvider::getOrder))
                    .collect(Collectors.toList());

            for (LiquibaseChangelogProvider provider : providers) {
                Liquibase liquibase = new Liquibase(provider.getChangeLogPath(), new ClassLoaderResourceAccessor(), database);
                liquibase.update(new Contexts(), new LabelExpression());
            }

        } finally {
            assert database != null;
            database.close();
        }
    }
}
