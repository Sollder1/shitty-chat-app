package de.sollder1.base.core.business.localization.internal;

import de.sollder1.base.core.business.localization.api.Translator;
import de.sollder1.base.core.business.localization.api.TranslatorBundle;
import de.sollder1.base.core.business.state.RequestBoundState;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.inject.Inject;

import java.util.Locale;
import java.util.ResourceBundle;

public class DefaultTranslator implements Translator {

    private final String bundleName;

    @Inject
    private TranslationCache translationCache;
    @Inject
    private RequestBoundState requestBoundState;


    @Inject
    public DefaultTranslator(InjectionPoint injectionPoint) {
        this.bundleName = injectionPoint.getAnnotated().getAnnotation(TranslatorBundle.class).value();
    }

    @Override
    public String get(String key, Object... params) {
        return getTranslation(key, params);
    }


    //TODO: actual Support for Params...
    private String getTranslation(String key, Object[] params) {

        if (!translationCache.getTranslationStorage().containsKey(bundleName)) {
            throw new RuntimeException("Bundle not Supported!");
        }

        var bundleTranslations = translationCache.getTranslationStorage().get(bundleName);

        if (!bundleTranslations.containsKey(requestBoundState.getRequestLocale())) {
            return "?__" + key + "__?";
        } else {
            return getBundleElement(key, bundleTranslations.get(requestBoundState.getRequestLocale()));
        }

    }

    private String getBundleElement(String key, ResourceBundle bundle) {
        if (bundle.containsKey(key)) {
            return bundle.getString(key);
        } else {
            return "?__" + key + "__?";
        }
    }

    public Locale getLocale() {
        return requestBoundState.getRequestLocale();
    }


}
