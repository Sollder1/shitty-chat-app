package de.sollder1.base.core.business.configuration.model;

public enum ConfigurationType {
    STRING, BOOLEAN, INTEGER, FLOATING_POINT
}