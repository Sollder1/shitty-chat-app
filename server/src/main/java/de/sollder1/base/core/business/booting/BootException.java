package de.sollder1.base.core.business.booting;

public class BootException extends Error {

    public BootException(String message) {
        super(message);
    }
}
