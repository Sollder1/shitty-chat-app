package de.sollder1.base.core.business.user.exception;

public class InvalidTokenException extends AuthenticationException {
    public InvalidTokenException() {
        super("InvalidTokenExceptionMessage");
    }
}
