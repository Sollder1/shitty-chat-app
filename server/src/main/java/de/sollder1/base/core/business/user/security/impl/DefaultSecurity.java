package de.sollder1.base.core.business.user.security.impl;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import de.sollder1.base.core.business.configuration.ConfigurationFacade;
import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import de.sollder1.base.core.business.user.account.AccountFacade;
import de.sollder1.base.core.business.user.entities.Account;
import de.sollder1.base.core.business.user.entities.AuthToken;
import de.sollder1.base.core.business.user.entities.PermissionEntity;
import de.sollder1.base.core.business.user.exception.*;
import de.sollder1.base.core.business.user.security.Security;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

@RequestScoped
public class DefaultSecurity implements Security {

    private static final HttpTransport TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new GsonFactory();
    private static final long TOKEN_LIFETIME = 1000 * 60 * 60 * 24 * 7;

    @Inject
    private ConfigurationFacade configurationFacade;
    @Inject
    private SecurityDao securityDao;
    @Inject
    private JpaManager jpa;
    @Inject
    private AccountFacade accountFacade;

    private boolean loggedIn;
    private Account loggedInAccount;
    private AuthToken usedToken;

    @Override
    public void validateAuthToken(String token) throws AuthenticationException {
        usedToken = securityDao.getAuthToken(token).orElseThrow(InvalidTokenException::new);
        loggedIn = true;
        loggedInAccount = usedToken.getAccount();
    }

    @Override
    @Transactional
    public AuthToken login(String username, String password) throws AuthenticationException {
        String normalizedUsername = username.toLowerCase();
        loggedInAccount = securityDao.getAccountByUserName(normalizedUsername).orElseThrow(UnknownAccountException::new);
        loggedIn = PasswordUtils.passwordsMatch(loggedInAccount.getPassword(), loggedInAccount.getSalt(), password);

        if (!loggedIn) {
            throw new PasswordNotMatchException();
        } else {
            return createToken();
            //TODO: some cleanup function to delete old tokens...!
        }
    }

    @Override
    @Transactional
    public AuthToken loginWithGoogle(String googleAuthToken) throws AuthenticationException {

        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(TRANSPORT, JSON_FACTORY)
                .setAudience(Collections.singletonList(configurationFacade.getItem("googleClientId").orElseThrow().asString()))
                .build();

        try {
            var idToken = verifier.verify(googleAuthToken);
            if (idToken == null) {
                throw new AuthenticationException("googleLoginFailedMessage");
            }
            String userId = idToken.getPayload().getSubject();
            var accountOptional = securityDao.getAccountGoogleUserId(userId);
            if (accountOptional.isEmpty()) {
                accountFacade.registerAccountByGoogle(googleAuthToken);
            }
            this.loggedInAccount = securityDao.getAccountGoogleUserId(userId).orElseThrow(UnknownAccountException::new);
            return createToken();
        } catch (GeneralSecurityException | IOException e) {
            throw new AuthenticationException("googleLoginFailedMessage");
        }
    }

    @Override
    @Transactional
    public void logout(boolean invalidateToken) {

        if (invalidateToken && this.usedToken != null) {
            jpa.delete(jpa.merge(this.usedToken));
        }

        loggedIn = false;
    }

    @Override
    public boolean isAdmin() {
        return this.loggedInAccount.isAdmin();
    }

    @Override
    public boolean isLoggedIn() {
        return loggedIn;
    }

    @Override
    public boolean hasPermission(String permission) throws NotLoggedInException {
        return isAdmin() || loggedInAccount.getRole().getPermissions().stream().map(PermissionEntity::getName)
                .anyMatch(permissionName -> permissionName.equals(permission));
    }


    @Override
    public Account getLoggedInAccount() throws NotLoggedInException {
        return loggedInAccount;
    }

    //##### Private Hilfsmethoden #####

    private AuthToken createToken() {

        var token = new AuthToken();
        token.setToken(UUID.randomUUID().toString());
        token.setExpirationDate(new Date(System.currentTimeMillis() + TOKEN_LIFETIME));
        token.setOriginIp("TODO!");

        token.setAccount(this.loggedInAccount);
        this.loggedInAccount.getAuthTokens().add(token);
        jpa.merge(loggedInAccount);

        return token;
    }

}
