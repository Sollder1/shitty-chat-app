package de.sollder1.base.core.business.persitence.db.jpa;

import jakarta.persistence.*;

import java.util.*;

public class TypeQueryWrapper<X> implements TypedQuery<X> {

    private final TypedQuery<X> queryToWrap;

    public TypeQueryWrapper(TypedQuery<X> queryToWrap) {
        this.queryToWrap = queryToWrap;
    }


    //Additional functions:

    /**
     * Execute a SELECT query that returns a optional single result.
     * If no result is found, no exception is thrown.
     *
     * @return The Optional result
     * @throws NonUniqueResultException     if more than one result
     * @throws IllegalStateException        if called for a Jakarta
     *                                      Persistence query language UPDATE or DELETE statement
     * @throws QueryTimeoutException        if the query execution exceeds
     *                                      the query timeout value set and only the statement is
     *                                      rolled back
     * @throws TransactionRequiredException if a lock mode other than
     *                                      <code>NONE</code> has been set and there is no transaction
     *                                      or the persistence context has not been joined to the
     *                                      transaction
     * @throws PessimisticLockException     if pessimistic locking
     *                                      fails and the transaction is rolled back
     * @throws LockTimeoutException         if pessimistic locking
     *                                      fails and only the statement is rolled back
     * @throws PersistenceException         if the query execution exceeds
     *                                      the query timeout value set and the transaction
     *                                      is rolled back
     */
    public Optional<X> getOptionalSingleResult() throws NonUniqueResultException {

        var result = this.getResultList();

        if (result.isEmpty()) {
            return Optional.empty();
        } else {
            if (result.size() > 1) {
                throw new NonUniqueResultException();
            } else {
                return Optional.of(result.get(0));
            }
        }
    }


    //All default functions get delegated...
    @Override
    public List<X> getResultList() {
        return queryToWrap.getResultList();
    }

    @Override
    public X getSingleResult() {
        return queryToWrap.getSingleResult();
    }

    @Override
    public int executeUpdate() {
        return queryToWrap.executeUpdate();
    }

    @Override
    public TypedQuery<X> setMaxResults(int maxResults) {
        return queryToWrap.setMaxResults(maxResults);
    }

    @Override
    public int getMaxResults() {
        return queryToWrap.getMaxResults();
    }

    @Override
    public TypedQuery<X> setFirstResult(int startPosition) {
        return queryToWrap.setFirstResult(startPosition);
    }

    @Override
    public int getFirstResult() {
        return queryToWrap.getFirstResult();
    }

    @Override
    public TypedQuery<X> setHint(String hintName, Object value) {
        return queryToWrap.setHint(hintName, value);
    }

    @Override
    public Map<String, Object> getHints() {
        return queryToWrap.getHints();
    }

    @Override
    public <T> TypedQuery<X> setParameter(Parameter<T> param, T value) {
        return queryToWrap.setParameter(param, value);
    }

    @Override
    public TypedQuery<X> setParameter(Parameter<Calendar> param, Calendar value, TemporalType temporalType) {
        return queryToWrap.setParameter(param, value, temporalType);
    }

    @Override
    public TypedQuery<X> setParameter(Parameter<Date> param, Date value, TemporalType temporalType) {
        return queryToWrap.setParameter(param, value, temporalType);
    }

    @Override
    public TypedQuery<X> setParameter(String name, Object value) {
        return queryToWrap.setParameter(name, value);
    }

    @Override
    public TypedQuery<X> setParameter(String name, Calendar value, TemporalType temporalType) {
        return queryToWrap.setParameter(name, value, temporalType);
    }

    @Override
    public TypedQuery<X> setParameter(String name, Date value, TemporalType temporalType) {
        return queryToWrap.setParameter(name, value, temporalType);
    }

    @Override
    public TypedQuery<X> setParameter(int position, Object value) {
        return queryToWrap.setParameter(position, value);
    }

    @Override
    public TypedQuery<X> setParameter(int position, Calendar value, TemporalType temporalType) {
        return queryToWrap.setParameter(position, value, temporalType);
    }

    @Override
    public TypedQuery<X> setParameter(int position, Date value, TemporalType temporalType) {
        return queryToWrap.setParameter(position, value, temporalType);
    }

    @Override
    public Set<Parameter<?>> getParameters() {
        return queryToWrap.getParameters();
    }

    @Override
    public Parameter<?> getParameter(String name) {
        return queryToWrap.getParameter(name);
    }

    @Override
    public <T> Parameter<T> getParameter(String name, Class<T> type) {
        return queryToWrap.getParameter(name, type);
    }

    @Override
    public Parameter<?> getParameter(int position) {
        return queryToWrap.getParameter(position);
    }

    @Override
    public <T> Parameter<T> getParameter(int position, Class<T> type) {
        return queryToWrap.getParameter(position, type);
    }

    @Override
    public boolean isBound(Parameter<?> param) {
        return queryToWrap.isBound(param);
    }

    @Override
    public <T> T getParameterValue(Parameter<T> param) {
        return queryToWrap.getParameterValue(param);
    }

    @Override
    public Object getParameterValue(String name) {
        return queryToWrap.getParameterValue(name);
    }

    @Override
    public Object getParameterValue(int position) {
        return queryToWrap.getParameterValue(position);
    }

    @Override
    public TypedQuery<X> setFlushMode(FlushModeType flushMode) {
        return queryToWrap.setFlushMode(flushMode);
    }

    @Override
    public FlushModeType getFlushMode() {
        return queryToWrap.getFlushMode();
    }

    @Override
    public TypedQuery<X> setLockMode(LockModeType lockMode) {
        return queryToWrap.setLockMode(lockMode);
    }

    @Override
    public LockModeType getLockMode() {
        return queryToWrap.getLockMode();
    }

    @Override
    public <T> T unwrap(Class<T> cls) {
        return queryToWrap.unwrap(cls);
    }
}
