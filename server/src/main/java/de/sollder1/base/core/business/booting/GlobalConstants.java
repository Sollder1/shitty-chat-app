package de.sollder1.base.core.business.booting;

import java.util.Locale;

public final class GlobalConstants {

    public static final Locale DEFAULT_LOCALE = Locale.GERMAN;

    private GlobalConstants() {
    }

}
