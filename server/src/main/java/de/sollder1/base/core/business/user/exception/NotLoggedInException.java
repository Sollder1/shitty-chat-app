package de.sollder1.base.core.business.user.exception;

public class NotLoggedInException extends AuthenticationException {

    public NotLoggedInException() {
        super("NotLoggedInExceptionMessage");
    }
}
