package de.sollder1.base.core.business.user.account;

import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import de.sollder1.base.core.business.user.entities.Account;
import de.sollder1.base.core.business.util.ListUtil;
import de.sollder1.base.core.business.user.entities.Account_;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import java.util.List;
import java.util.Optional;

@Singleton
public class AccountDao {

    @Inject
    private JpaManager jpa;

    public Optional<Account> getAccountById(String id) {
        return jpa.get(Account.class, id);
    }

    public List<Account> queryAccounts(String query) {
        final String template = "%" + query.toLowerCase() + "%";
        return jpa.buildTypeQuery((cb, root, cq) -> {
            return ListUtil.get(
                    cb.like(cb.lower(root.get(Account_.username)), template)
            );
        }, Account.class).getResultList();

    }

}
