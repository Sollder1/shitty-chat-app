package de.sollder1.base.core.business.localization.internal;


import de.sollder1.base.core.business.booting.BootException;
import de.sollder1.base.core.business.localization.api.TranslationProvider;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import org.tinylog.Logger;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

@ApplicationScoped
public class TranslationCache {

    private final Map<String, Map<Locale, ResourceBundle>> translationStorage = new HashMap<>();
    @Inject
    private Instance<TranslationProvider> translationProviders;
    private boolean initialized;

    public synchronized void init() {

        if (initialized) {
            throw new BootException("TranslationCache already initialised!");
        }
        initialized = true;

        if (translationProviders.isUnsatisfied()) {
            Logger.warn("No Translations provided!");
        }
        translationProviders.forEach(translationProvider -> {
            translationProvider.getBundleNames().forEach(bundleName -> {
                translationStorage.put(bundleName, buildBundleMap(bundleName, translationProvider));
            });
        });

    }

    private Map<Locale, ResourceBundle> buildBundleMap(String bundleName, TranslationProvider translationProvider) {
        Map<Locale, ResourceBundle> bundleTranslationMap = new HashMap<>();

        translationProvider.getSupportedLocales().forEach(locale -> {
            bundleTranslationMap.put(locale, translationProvider.getResourceBundle(bundleName, locale));
        });

        return bundleTranslationMap;

    }

    protected Map<String, Map<Locale, ResourceBundle>> getTranslationStorage() {
        return translationStorage;
    }
}
