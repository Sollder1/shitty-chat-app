package de.sollder1.base.core.processing.messagemodel.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BaseResponsePayload {

    private String id;

    private String createdBy;
    private String lastUpdatedBy;

    private Long createdAt;
    private Long lastUpdatedAt;

}
