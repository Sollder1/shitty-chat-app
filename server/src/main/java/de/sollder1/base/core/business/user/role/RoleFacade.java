package de.sollder1.base.core.business.user.role;

import de.sollder1.base.core.business.user.entities.RoleEntity;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import java.util.List;
import java.util.Optional;

@Singleton
public class RoleFacade {

    @Inject
    private RoleDao roleDao;

    public List<RoleEntity> getAll() {
        return roleDao.getJpa().getAll(RoleEntity.class);
    }

    public Optional<RoleEntity> getById(String id) {
        return roleDao.getJpa().get(RoleEntity.class, id);
    }

}
