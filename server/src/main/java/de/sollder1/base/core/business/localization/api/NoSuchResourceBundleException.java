package de.sollder1.base.core.business.localization.api;

public class NoSuchResourceBundleException extends RuntimeException {

    public NoSuchResourceBundleException(String message) {
        super(message);
    }
}
