package de.sollder1.base.core.business.logging;

import org.tinylog.Level;
import org.tinylog.core.LogEntry;
import org.tinylog.core.LogEntryValue;
import org.tinylog.pattern.FormatPatternParser;
import org.tinylog.pattern.Token;
import org.tinylog.provider.InternalLogger;
import org.tinylog.writers.AbstractWriter;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class DbWriter extends AbstractWriter {

    private static final int BATCH_MIN_SIZE = 1;
    private static final int BATCH_MAX_SIZE = 4096;
    private static final String FIELD_PREFIX = "field.";

    //Maybe switch to ConcurrentLinkedQueue later...
    private final BlockingQueue<LogEntry> queue = new LinkedBlockingQueue<>(128_000);
    private Connection connection;
    private final String sql;
    private final List<Token> tokens;


    /**
     * @param properties Configuration for writer
     */
    public DbWriter(Map<String, String> properties) throws SQLException {
        super(properties);
        openConnectionIfClosed();
        this.sql = renderSql(properties, connection.getMetaData().getIdentifierQuoteString());
        this.tokens = createTokens(properties);
        this.spawnFlushDaemon();
        FlushLogsListener.registerLoggingWriter(this);
    }


    @Override
    public void write(LogEntry logEntry) throws InterruptedException {
        queue.put(logEntry);
    }

    @Override
    public void flush() {
        doFlush(BATCH_MIN_SIZE, BATCH_MAX_SIZE);
    }

    private void doFlush(int batchMinSize, int batchMaxSize) {
        if (this.queue.size() < batchMinSize) {
            return;
        }

        Collection<LogEntry> batchToWrite = new LinkedList<>();
        this.getQueue().drainTo(batchToWrite, batchMaxSize);

        openConnectionIfClosed();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (var logEntry : batchToWrite) {
                for (int i = 0; i < tokens.size(); ++i) {
                    tokens.get(i).apply(logEntry, statement, i + 1);
                }
                statement.addBatch();
            }
            statement.executeBatch();
        } catch (SQLException e) {
            //Add them back to the queue if it fails...
            queue.addAll(batchToWrite);
            InternalLogger.log(Level.ERROR, e);
        }
    }

    @Override
    public void close() {

        try {
            InternalLogger.log(Level.INFO, "Flushing remaining logs...");
            if (connection != null && !connection.isClosed()) {
                this.doFlush(0, Integer.MAX_VALUE);
                connection.close();
            }
        } catch (SQLException e) {
            InternalLogger.log(Level.ERROR, "Could not close Logger by flushing content to DB.... Logs lost: " + this.queue.size());
            InternalLogger.log(Level.ERROR, e);
        }


    }

    public BlockingQueue<LogEntry> getQueue() {
        return queue;
    }

    private void openConnectionIfClosed() {
        try {
            if (connection == null || connection.isClosed()) {
                DataSource source = (DataSource) new InitialContext().lookup("java:comp/env/yfm-none-jta");
                this.connection = source.getConnection();
            }
        } catch (SQLException | NamingException e) {
            InternalLogger.log(Level.ERROR, e);
        }
    }

    private void spawnFlushDaemon() {
        Thread thread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                this.flush();
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    break;
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    //Stolen from JdbcWriter:

    @Override
    public Collection<LogEntryValue> getRequiredLogEntryValues() {
        Collection<LogEntryValue> values = EnumSet.noneOf(LogEntryValue.class);
        for (Token token : tokens) {
            values.addAll(token.getRequiredLogEntryValues());
        }
        return values;
    }

    private static String renderSql(final Map<String, String> properties, final String quote) throws SQLException {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO ");
        if (properties.get("schema") != null) {
            append(builder, properties.get("schema"), quote);
            builder.append(".");
        }
        append(builder, getTable(properties), quote);
        builder.append(" (");

        int count = 0;

        for (Map.Entry<String, String> entry : properties.entrySet()) {
            String key = entry.getKey();
            if (key.toLowerCase(Locale.ROOT).startsWith(FIELD_PREFIX)) {
                String column = key.substring(FIELD_PREFIX.length());
                if (count++ != 0) {
                    builder.append(", ");
                }
                append(builder, column, quote);
            }
        }

        builder.append(") VALUES (");

        for (int i = 0; i < count; ++i) {
            if (i > 0) {
                builder.append(", ?");
            } else {
                builder.append("?");
            }
        }

        builder.append(")");

        return builder.toString();
    }

    /**
     * Appends a database identifier securely to a builder that is building a SQL statement.
     *
     * @param builder    String builder that is building a SQL statement
     * @param identifier Identifier to add
     * @param quote      Character for quoting the identifier (can be a space if the database doesn't support quote characters)
     * @throws SQLException Identifier contains an illegal character
     */
    private static void append(final StringBuilder builder, final String identifier, final String quote) throws SQLException {
        if (identifier.indexOf('\n') >= 0 || identifier.indexOf('\r') >= 0) {
            throw new SQLException("Identifier contains line breaks: " + identifier);
        } else if (" ".equals(quote)) {
            for (int i = 0; i < identifier.length(); ++i) {
                char c = identifier.charAt(i);
                if (!Character.isLetterOrDigit(c) && c != '_' && c != '@' && c != '$' && c != '#') {
                    throw new SQLException("Illegal identifier: " + identifier);
                }
            }
            builder.append(identifier);
        } else {
            builder.append(quote).append(identifier.replace(quote, quote + quote)).append(quote);
        }
    }

    /**
     * Extracts the database table name from configuration.
     *
     * @param properties Configuration for writer
     * @return Name of database table
     * @throws IllegalArgumentException Table is not defined in configuration
     */
    private static String getTable(final Map<String, String> properties) {
        String table = properties.get("table");
        if (table == null) {
            throw new IllegalArgumentException("Name of database table is missing for JDBC writer");
        } else {
            return table;
        }
    }

    /**
     * Creates tokens for all configured fields.
     *
     * @param properties Properties that contains the configured fields
     * @return Tokens for filling a {@link PreparedStatement}
     */
    private static List<Token> createTokens(final Map<String, String> properties) {
        FormatPatternParser parser = new FormatPatternParser(properties.get("exception"));

        List<Token> tokens = new ArrayList<>();
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            if (entry.getKey().toLowerCase(Locale.ROOT).startsWith(FIELD_PREFIX)) {
                tokens.add(parser.parse(entry.getValue()));
            }
        }
        return tokens;
    }
}
