package de.sollder1.base.core.business.user.exception;

public class UnknownAccountException extends AuthenticationException {
    public UnknownAccountException() {
        super("UnknownAccountExceptionMessage");
    }
}
