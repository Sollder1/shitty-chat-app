package de.sollder1.base.core.databinding.impl.databinder.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.sollder1.base.core.databinding.api.DataBindingException;
import de.sollder1.base.core.databinding.api.databinder.RequestDataBinder;
import de.sollder1.base.core.processing.messagemodel.request.RestRequest;


public class JsonRequestDataBinder<T> implements RequestDataBinder<T> {

    @Override
    public T bind(RestRequest toBind) throws DataBindingException {

        try {
            Object result = JsonMapperPool.getMapper().readValue((String) toBind.getRawRequest().getPayloadRaw(), toBind.getPayloadClazz());
            return (T) result;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new DataBindingException(400, "RequestNotParsable");
        }

    }
}
