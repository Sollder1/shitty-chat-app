package de.sollder1.base.core.databinding.impl.exceptions;

import de.sollder1.base.core.databinding.api.DataBindingException;

public class DatabindingAuthException extends DataBindingException {

    public DatabindingAuthException(int responseCode, String error) {
        super(responseCode, error);
    }
}
