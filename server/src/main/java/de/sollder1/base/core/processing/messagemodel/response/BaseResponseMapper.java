package de.sollder1.base.core.processing.messagemodel.response;

import de.sollder1.base.core.business.persitence.db.entities.BaseEntity;
import de.sollder1.base.core.business.util.DateTimeUtil;

public class BaseResponseMapper {

    public static BaseResponsePayload mapToPayload(BaseResponsePayload payload, BaseEntity entity) {

        payload.setId(entity.getId());
        payload.setCreatedBy(entity.getCreatedBy());
        payload.setLastUpdatedBy(entity.getLastChangedBy());
        payload.setCreatedAt(DateTimeUtil.to(entity.getCreatedAt()));
        payload.setLastUpdatedAt(DateTimeUtil.to(entity.getLastChangedAt()));

        return payload;
    }

}
