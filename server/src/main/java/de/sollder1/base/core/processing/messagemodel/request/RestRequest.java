package de.sollder1.base.core.processing.messagemodel.request;

import jakarta.ws.rs.core.MultivaluedMap;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public abstract class RestRequest<T> {

    //The Raw params of the request
    private RawRequest rawRequest;
    private T payload;

    public Class<T> getPayloadClazz() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public RawRequest getRawRequest() {
        return rawRequest;
    }

    public void setRawRequest(RawRequest rawRequest) {
        this.rawRequest = rawRequest;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    private Optional<String> getParamInternal(String key, MultivaluedMap<String, String> target) {
        List<String> values = target.get(key);

        if (values == null || values.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(values.get(0));
    }


    public Optional<String> getHeaderOptional(String key) {
        return getParamInternal(key, rawRequest.getRequestHeaders());
    }

    public Optional<String> getQueryParamOptional(String key) {
        return getParamInternal(key, rawRequest.getRequestQueryParams());
    }

    public Optional<String> getPathParamOptional(String key) {
        return getParamInternal(key, rawRequest.getRequestPathParams());
    }

    public String getHeader(String key) throws NoSuchElementException {
        return getParamInternal(key, rawRequest.getRequestHeaders()).orElseThrow();
    }

    public String getQueryParam(String key) throws NoSuchElementException {
        return getParamInternal(key, rawRequest.getRequestQueryParams()).orElseThrow();
    }

    public String getPathParam(String key) throws NoSuchElementException {
        return getParamInternal(key, rawRequest.getRequestPathParams()).orElseThrow();
    }

}
