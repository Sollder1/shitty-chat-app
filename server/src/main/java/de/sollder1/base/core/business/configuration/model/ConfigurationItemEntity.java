package de.sollder1.base.core.business.configuration.model;

import de.sollder1.base.core.business.persitence.db.entities.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

@Entity(name = "config_item")
public class ConfigurationItemEntity extends BaseEntity {

    private String identifier;
    @Column(name = "raw_value")
    private String rawValue;
    @Enumerated(EnumType.STRING)
    private ConfigurationType type;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public ConfigurationType getType() {
        return type;
    }

    public void setType(ConfigurationType type) {
        this.type = type;
    }

}
