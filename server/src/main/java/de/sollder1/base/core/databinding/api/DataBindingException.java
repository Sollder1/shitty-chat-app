package de.sollder1.base.core.databinding.api;

/**
 * Represents a anticipated Exception during Databinding, so pre Processing...
 */
public class DataBindingException extends Exception {

    private transient final int responseCode;
    private transient final String error;

    public DataBindingException(int responseCode, String error) {
        this.responseCode = responseCode;
        this.error = error;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getError() {
        return error;
    }
}
