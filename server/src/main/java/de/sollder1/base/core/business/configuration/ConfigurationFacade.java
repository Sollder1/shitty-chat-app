package de.sollder1.base.core.business.configuration;

import de.sollder1.base.core.business.configuration.dao.ConfigurationDao;
import de.sollder1.base.core.business.configuration.model.ConfigurationItem;
import jakarta.inject.Inject;

import java.util.Optional;

public class ConfigurationFacade {

    @Inject
    private ConfigurationDao configurationDao;

    public Optional<ConfigurationItem> getItem(String identifier) {
        var value = configurationDao.getByIdentifier(identifier);
        if (value == null) {
            return Optional.empty();
        }
        return Optional.of(new ConfigurationItem(value));
    }

}
