package de.sollder1.base.core.business.exceptions;

public class ReportableException extends RuntimeException {

    public ReportableException() {
    }

    public ReportableException(String message) {
        super(message);
    }

    public ReportableException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReportableException(Throwable cause) {
        super(cause);
    }

}
