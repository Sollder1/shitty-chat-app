package de.sollder1.base.core.databinding.impl.databinder.json;

import de.sollder1.base.core.processing.messagemodel.defaults.EmptyPayload;
import de.sollder1.base.core.databinding.api.databinder.ResponseDataBinder;
import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class JsonResponseDataBinder implements ResponseDataBinder {

    @Override
    public Response bind(RestResponse response) throws Exception {

        if (response.hasFailed()) {
            return bindError(response.getStatusCode(), response.getError());
        }

        var builder = internalGeneration(response.getStatusCode());
        if (!(response.getPayload() instanceof EmptyPayload) && response.getPayload() != null) {
            builder.entity(JsonMapperPool.getMapper().writeValueAsString(response.getPayload()))
                    .type(MediaType.APPLICATION_JSON);
        }

        return builder.build();
    }

    @Override
    public Response bindError(int statusCode) {
        return internalGeneration(statusCode).entity("{}").build();
    }

    @Override
    public Response bindError(int statusCode, String error) throws Exception {
        return internalGeneration(statusCode)
                .entity(JsonMapperPool.getMapper().writeValueAsString(new ErrorPayload(error)))
                .build();
    }

    private Response.ResponseBuilder internalGeneration(int statusCode) {
        return Response.status(statusCode)
                .encoding("UTF-8");
    }
}
