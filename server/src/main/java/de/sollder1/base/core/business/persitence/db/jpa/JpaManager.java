package de.sollder1.base.core.business.persitence.db.jpa;

import de.sollder1.base.core.business.persitence.db.entities.AccountBoundEntity;
import de.sollder1.base.core.business.persitence.db.entities.Persistable;
import jakarta.persistence.EntityManager;

import java.util.List;
import java.util.Optional;

public interface JpaManager {

    <T extends Persistable> void persist(T toPersist);

    <T extends Persistable> T merge(T toMerge);

    <T extends Persistable> void delete(T toDelete);

    <T> Optional<T> get(Class<T> entityClazz, String primaryKey);

    <T> List<T> getAll(Class<T> entityClazz);

    <T extends Persistable> TypeQueryWrapper<T> buildTypeQuery(PredicateBuilder<T> builder, Class<T> entityClazz);

    <T extends AccountBoundEntity> TypeQueryWrapper<T> buildTypeQueryAccountBound(PredicateBuilder<T> builder, Class<T> entityClazz);

    EntityManager getEm();

}
