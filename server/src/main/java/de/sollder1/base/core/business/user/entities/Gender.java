package de.sollder1.base.core.business.user.entities;

public enum Gender {
    MALE, FEMALE, OTHER, UNKNOWN
}
