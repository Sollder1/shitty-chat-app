package de.sollder1.base.core.databinding.api.databinder;

import de.sollder1.base.core.databinding.api.DataBindingException;
import de.sollder1.base.core.processing.messagemodel.request.RestRequest;

public interface RequestDataBinder<T> {
    T bind(RestRequest toBind) throws DataBindingException;
}
