package de.sollder1.base.core.business.configuration.dao;

import de.sollder1.base.core.business.configuration.model.ConfigurationItemEntity;
import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import de.sollder1.base.core.business.persitence.db.jpa.impl.AsyncSave;
import jakarta.inject.Inject;

import java.util.Collections;

public class ConfigurationDao {

    @Inject
    @AsyncSave
    private JpaManager jpa;
    
    public ConfigurationItemEntity getByIdentifier(String identifier) {
        return jpa.buildTypeQuery((cb, root, cq) ->
                        Collections.singletonList(cb.equal(root.get("identifier"), identifier))
                , ConfigurationItemEntity.class).getSingleResult();
    }


}
