package de.sollder1.base.core.business.persitence.db.jpa;


import de.sollder1.base.core.business.persitence.db.entities.Persistable;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import java.util.List;

public interface PredicateBuilder<T extends Persistable> {
    List<Predicate> build(CriteriaBuilder cb, Root<T> root, CriteriaQuery<T> cq);
}
