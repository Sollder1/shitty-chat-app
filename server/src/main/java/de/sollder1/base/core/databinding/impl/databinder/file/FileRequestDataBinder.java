package de.sollder1.base.core.databinding.impl.databinder.file;

import de.sollder1.base.core.databinding.api.DataBindingException;
import de.sollder1.base.core.databinding.api.databinder.RequestDataBinder;
import de.sollder1.base.plugins.files.messagemodel.FileRequestPayload;
import de.sollder1.base.core.processing.messagemodel.request.RestRequest;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;

public class FileRequestDataBinder implements RequestDataBinder<FileRequestPayload> {

    @Override
    public FileRequestPayload bind(RestRequest toBind) throws DataBindingException {

        if (toBind.getRawRequest().getPayloadRaw().getClass() != Attachment.class) {
            throw new DataBindingException(415, "Can only handle org.apache.cxf.jaxrs.ext.multipart.Attachment as payload");
        }
        return new FileRequestPayload((Attachment) toBind.getRawRequest().getPayloadRaw());
    }
}
