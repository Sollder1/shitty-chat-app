package de.sollder1.base.core.connection;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("rest")
public class RestApplication extends Application {

}