package de.sollder1.base.core.business.state;

import jakarta.enterprise.context.RequestScoped;

import java.util.Locale;

@RequestScoped
public class RequestBoundState {

    private Locale requestLocale;

    public Locale getRequestLocale() {
        return requestLocale;
    }

    public void setRequestLocale(Locale requestLocale) {
        this.requestLocale = requestLocale;
    }
}
