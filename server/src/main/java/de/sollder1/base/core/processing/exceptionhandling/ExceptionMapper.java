package de.sollder1.base.core.processing.exceptionhandling;

import java.util.Optional;
import java.util.OptionalInt;

public interface ExceptionMapper {

    Optional<String> getError(Exception e);

    OptionalInt getStatusCode(Exception e);

}
