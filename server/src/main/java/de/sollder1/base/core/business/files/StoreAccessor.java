package de.sollder1.base.core.business.files;

import de.sollder1.base.core.business.configuration.model.ConfigurationItem;
import de.sollder1.base.core.business.configuration.ConfigurationFacade;
import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@ApplicationScoped
public class StoreAccessor {

    private String storeBasePath = "";

    @Inject
    private JpaManager jpa;
    @Inject
    private ConfigurationFacade configuration;

    @PostConstruct
    private void init() {
        storeBasePath = configuration.getItem("store_base_path")
                .map(ConfigurationItem::asString).orElse(System.getProperty("java.io.tmpdir"));
    }

    @Transactional
    public FileEntity createFile(InputStream data, String fileName) throws IOException {

        String extension = getFileExtension(fileName);

        FileEntity entity = new FileEntity();
        entity.setFilename(fileName);
        Path target = Paths.get(storeBasePath, entity.getId() + "." + extension);
        entity.setStorePath(target.toString());
        entity.setFileExtension(extension);
        //TODO...
        entity.setMimeType(null);

        Files.copy(data, target);
        entity.setSizeInBytes(Files.size(target));
        jpa.persist(entity);

        return entity;
    }

    private String getFileExtension(String fileName) {
        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i >= 0) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }


    public File loadFile(String storePath) {
        return Paths.get(storePath).toFile();
    }

}
