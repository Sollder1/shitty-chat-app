package de.sollder1.base.core.business.configuration.model;

import de.sollder1.base.core.business.configuration.exception.ConfigurationTypeException;

public class ConfigurationItem {

    private final ConfigurationItemEntity entity;

    public ConfigurationItem(ConfigurationItemEntity entity) {
        this.entity = entity;
    }

    public String asString() {
        if (entity.getType() != ConfigurationType.STRING) {
            throw new ConfigurationTypeException(entity.getType(), ConfigurationType.STRING);
        }
        return entity.getRawValue();
    }

    public Boolean asBoolean() {
        if (entity.getType() != ConfigurationType.BOOLEAN) {
            throw new ConfigurationTypeException(entity.getType(), ConfigurationType.BOOLEAN);
        }
        return Boolean.parseBoolean(entity.getRawValue());
    }

    public Integer asInteger() {
        if (entity.getType() != ConfigurationType.INTEGER) {
            throw new ConfigurationTypeException(entity.getType(), ConfigurationType.INTEGER);
        }
        return Integer.parseInt(entity.getRawValue());
    }

    public Double asFloatingPoint() {
        if (entity.getType() != ConfigurationType.FLOATING_POINT) {
            throw new ConfigurationTypeException(entity.getType(), ConfigurationType.FLOATING_POINT);
        }
        return Double.parseDouble(entity.getRawValue());
    }

}
