package de.sollder1.base.core.processing.exceptionhandling;

import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;

public class ExceptionManager {

    @Inject
    private Instance<ExceptionMapper> exceptionMappers;

    public RestResponse map(Exception e) {

        String errorMessage = null;
        int errorCode = 500;

        for (ExceptionMapper mapper : exceptionMappers) {
            var optionalStatusCode = mapper.getStatusCode(e);
            if (optionalStatusCode.isPresent()) {
                errorCode = optionalStatusCode.getAsInt();
            }

            var optionalMessage = mapper.getError(e);
            if (optionalMessage.isPresent()) {
                errorMessage = optionalMessage.get();
            }
        }

        RestResponse response = new RestResponse();
        response.setError(errorMessage);
        response.setStatusCode(errorCode);

        return response;

    }

}
