package de.sollder1.base.core.databinding.impl.databinder.file;


import de.sollder1.base.core.databinding.api.databinder.ResponseDataBinder;
import de.sollder1.base.core.databinding.impl.databinder.json.JsonResponseDataBinder;
import de.sollder1.base.plugins.files.messagemodel.FileResponsePayload;
import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import jakarta.ws.rs.core.Response;

import java.io.FileInputStream;

public class FileResponseDataBinder implements ResponseDataBinder {


    @Override
    public Response bind(RestResponse response) throws Exception {
        FileResponsePayload payload = (FileResponsePayload) response.getPayload();
        return Response.ok().entity(new FileInputStream(payload.getFile())).build();
    }

    @Override
    public Response bindError(int statusCode) {
        JsonResponseDataBinder default_ = new JsonResponseDataBinder();
        return default_.bindError(statusCode);
    }

    @Override
    public Response bindError(int statusCode, String error) throws Exception {
        JsonResponseDataBinder default_ = new JsonResponseDataBinder();
        return default_.bindError(statusCode, error);
    }
}
