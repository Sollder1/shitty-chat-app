package de.sollder1.base.core.business.persitence.db.entities;

import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

public class EntityListener {

    @PrePersist
    void onPrePersist(BaseEntity baseEntity) {
        //TODO...
    }

    @PreUpdate
    void onPreUpdate(BaseEntity baseEntity) {
        //TODO...
    }

}
