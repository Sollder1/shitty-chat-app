package de.sollder1.base.core.databinding.api.databinder;


import de.sollder1.base.core.processing.messagemodel.response.RestResponse;
import jakarta.ws.rs.core.Response;

public interface ResponseDataBinder {
    Response bind(RestResponse response) throws Exception;

    Response bindError(int statusCode);

    Response bindError(int statusCode, String error) throws Exception;

}
