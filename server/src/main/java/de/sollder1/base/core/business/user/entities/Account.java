package de.sollder1.base.core.business.user.entities;

import de.sollder1.base.core.business.persitence.db.entities.BaseEntity;
import jakarta.persistence.*;

import java.util.Set;

@Entity(name = "account")
public class Account extends BaseEntity {

    private String username;
    private String password;
    private String salt;
    private boolean blocked;
    private boolean admin;

    @Column(name = "block_reason")
    private String blockReason;
    @Column(name = "google_user_id")
    private String googleUserId;
    @JoinColumn(name = "person_id")
    @ManyToOne(cascade = CascadeType.ALL)
    private Person assignedPerson;
    @JoinColumn(name = "role_id")
    @ManyToOne
    private RoleEntity role;
    @JoinColumn(name = "account_id")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<AuthToken> authTokens;

    @Transient
    private boolean changedPassword;

    public Account() {
    }

    public Account(Person person) {
        setAssignedPerson(person);
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getBlockReason() {
        return blockReason;
    }

    public void setBlockReason(String blockReason) {
        this.blockReason = blockReason;
    }

    public String getGoogleUserId() {
        return googleUserId;
    }

    public void setGoogleUserId(String googleUserId) {
        this.googleUserId = googleUserId;
    }

    public Person getAssignedPerson() {
        return assignedPerson;
    }

    public void setAssignedPerson(Person assignedPerson) {
        this.assignedPerson = assignedPerson;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    public Set<AuthToken> getAuthTokens() {
        return authTokens;
    }

    public void setAuthTokens(Set<AuthToken> authTokens) {
        this.authTokens = authTokens;
    }

    public boolean isChangedPassword() {
        return changedPassword;
    }

    public void setChangedPassword(boolean changedPassword) {
        this.changedPassword = changedPassword;
    }
}
