package de.sollder1.base.core.business.persitence.db.entities;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(EntityListener.class)
public abstract class BaseEntity extends Persistable {

    @Column(name = "created_at")
    private Date createdAt;
    @Column(name = "last_changed_at")
    private Date lastChangedAt;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "last_changed_by")
    private String lastChangedBy;

    public BaseEntity() {
        super();
        createdAt = new Date();
        lastChangedAt = new Date();
    }

}
