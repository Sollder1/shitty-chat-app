package de.sollder1.base.core.connection;

import de.sollder1.base.core.databinding.api.databinder.DataBindingHandler;
import de.sollder1.base.core.databinding.api.databinder.RequestDataBinder;
import de.sollder1.base.core.databinding.api.databinder.ResponseDataBinder;
import de.sollder1.base.core.databinding.impl.databinder.json.JsonResponseDataBinder;
import de.sollder1.base.core.processing.messagemodel.request.RawRequest;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.core.databinding.impl.databinder.json.JsonRequestDataBinder;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;


public class BasicEndpoint {

    @Inject
    private DataBindingHandler dataBindingHandler;

    @Context
    private HttpHeaders httpHeaders;
    @Context
    private UriInfo uriInfo;

    public <T extends Processor> Response handle(Class<T> processorClass) {
        RawRequest rawRequest = buildBaseRequest("{}");
        return dataBindingHandler.bind(new JsonRequestDataBinder<>(), new JsonResponseDataBinder(), rawRequest, processorClass);
    }

    public <T extends Processor> Response handle(String payload, Class<T> processorClass) {
        RawRequest rawRequest = buildBaseRequest(payload);
        return dataBindingHandler.bind(new JsonRequestDataBinder<>(), new JsonResponseDataBinder(), rawRequest, processorClass);
    }

    public <T extends Processor> Response handle(Object payload, Class<T> processorClass, RequestDataBinder requestDataBinder) {
        RawRequest rawRequest = buildBaseRequest(payload);
        return dataBindingHandler.bind(requestDataBinder, new JsonResponseDataBinder(), rawRequest, processorClass);
    }

    public <T extends Processor> Response handle(Object payload, Class<T> processorClass, ResponseDataBinder responseDataBinder) {
        RawRequest rawRequest = buildBaseRequest(payload);
        return dataBindingHandler.bind(new JsonRequestDataBinder(), responseDataBinder, rawRequest, processorClass);
    }

    private RawRequest buildBaseRequest(Object payload) {
        RawRequest rawRequest = new RawRequest();

        rawRequest.setRequestHeaders(httpHeaders.getRequestHeaders());
        rawRequest.setRequestQueryParams(uriInfo.getQueryParameters());
        rawRequest.setRequestPathParams(uriInfo.getPathParameters());
        rawRequest.setPayloadRaw(payload);
        return rawRequest;
    }


}
