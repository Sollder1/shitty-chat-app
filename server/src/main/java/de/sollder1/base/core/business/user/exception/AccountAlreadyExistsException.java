package de.sollder1.base.core.business.user.exception;

import de.sollder1.base.core.business.exceptions.ReportableException;

public class AccountAlreadyExistsException extends ReportableException {

    public AccountAlreadyExistsException() {
        super("accountAlreadyExistsExceptionMessage");
    }

}
