package de.sollder1.base.core.business.user.entities;

import de.sollder1.base.core.business.persitence.db.entities.Persistable;
import jakarta.persistence.Entity;


@Entity(name = "permission")
public class PermissionEntity extends Persistable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
