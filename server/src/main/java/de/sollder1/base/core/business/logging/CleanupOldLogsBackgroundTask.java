package de.sollder1.base.core.business.logging;

import de.sollder1.base.core.business.configuration.ConfigurationFacade;
import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import de.sollder1.base.core.business.persitence.db.jpa.impl.AsyncSave;
import jakarta.ejb.Schedule;
import jakarta.ejb.Singleton;
import jakarta.inject.Inject;
import jakarta.persistence.Query;
import org.tinylog.Logger;

import java.util.Date;

@Singleton
public class CleanupOldLogsBackgroundTask {

    private static final String SQL = createSql();

    @Inject
    @AsyncSave
    private JpaManager jpa;
    @Inject
    private ConfigurationFacade configuration;

    private long now;

    @Schedule(hour = "3", info = "CleanupOldLogsBackgroundTask")
    private void task() {

        Logger.info("Start executing CleanupOldLogsBackgroundTask...");

        now = System.currentTimeMillis();
        var query = jpa.getEm().createNativeQuery(SQL);
        setParams(query);

        var updates = query.executeUpdate();
        Logger.info("Deleted {} entries", updates);

    }

    //Todo: maybe config param
    private void setParams(Query query) {
        query.setParameter(1, getMaxDate("logs.cleanup_delay_minutes.debug"));
        query.setParameter(2, getMaxDate("logs.cleanup_delay_minutes.info"));
        query.setParameter(3, getMaxDate("logs.cleanup_delay_minutes.warn"));
        query.setParameter(4, getMaxDate("logs.cleanup_delay_minutes.error"));
    }


    private Date getMaxDate(String paramName) {
        int deltaMinutes = configuration.getItem(paramName).orElseThrow(NoSuchFieldError::new).asInteger();
        long maxTimestamp = now - (deltaMinutes * 60L * 1000L);

        Logger.debug("Value for param {} is {}", paramName, deltaMinutes);

        return new Date(maxTimestamp);
    }


    private static String createSql() {
        return "DELETE FROM MONITORING.TINYLOG_LOGS WHERE " + " (level = 'TRACE') OR " +
                " (level = 'DEBUG' AND date < ?) OR " +
                " (level = 'INFO' AND date < ?) OR " +
                " (level = 'WARN' AND date < ?) OR " +
                " (level = 'ERROR' AND date < ?);";
    }

}
