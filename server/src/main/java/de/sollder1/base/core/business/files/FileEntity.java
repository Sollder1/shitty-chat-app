package de.sollder1.base.core.business.files;

import de.sollder1.base.core.business.persitence.db.entities.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;

@Entity(name = "file")
public class FileEntity extends BaseEntity {

    private String filename;
    @Column(name = "store_path")
    private String storePath;
    @Column(name = "size_in_bytes")
    private long sizeInBytes;
    @Column(name = "file_extension")
    private String fileExtension;
    @Column(name = "mime_type")
    private String mimeType;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getStorePath() {
        return storePath;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath;
    }

    public long getSizeInBytes() {
        return sizeInBytes;
    }

    public void setSizeInBytes(long sizeInBytes) {
        this.sizeInBytes = sizeInBytes;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
