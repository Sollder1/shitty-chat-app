package de.sollder1.base.core.business.persitence.db.jpa.impl;

import de.sollder1.base.core.business.user.security.Security;
import de.sollder1.base.core.business.persitence.db.entities.AccountBoundEntity;
import de.sollder1.base.core.business.persitence.db.entities.AccountBoundEntity_;
import de.sollder1.base.core.business.persitence.db.entities.Persistable;
import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import de.sollder1.base.core.business.persitence.db.jpa.PredicateBuilder;
import de.sollder1.base.core.business.persitence.db.jpa.TypeQueryWrapper;
import jakarta.inject.Inject;
import jakarta.inject.Provider;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import java.util.List;
import java.util.Optional;

public abstract class JpaManagerAdapter implements JpaManager {

    @Inject
    private Provider<Security> security;

    public <T extends Persistable> void persist(T toPersist) {
        getEm().persist(toPersist);
    }

    public <T extends Persistable> T merge(T toMerge) {
        return getEm().merge(toMerge);
    }

    public <T extends Persistable> void delete(T toDelete) {
        getEm().remove(toDelete);
    }

    public <T> Optional<T> get(Class<T> entityClazz, String primaryKey) {
        return Optional.ofNullable(getEm().find(entityClazz, primaryKey));
    }

    public <T> List<T> getAll(Class<T> entityClazz) {

        CriteriaBuilder cb = getEm().getCriteriaBuilder();
        CriteriaQuery<T> q = cb.createQuery(entityClazz);
        Root<T> root = q.from(entityClazz);
        q.select(root);

        TypedQuery<T> query = getEm().createQuery(q);
        return query.getResultList();

    }

    public <T extends Persistable> TypeQueryWrapper<T> buildTypeQuery(PredicateBuilder<T> builder, Class<T> entityClazz) {
        CriteriaBuilder cb = getEm().getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClazz);
        Root<T> root = cq.from(entityClazz);

        List<Predicate> predicates = builder.build(cb, root, cq);

        cq.where(predicates.toArray(new Predicate[0]));
        cq.select(root);

        return new TypeQueryWrapper<>(getEm().createQuery(cq));
    }

    public <T extends AccountBoundEntity> TypeQueryWrapper<T> buildTypeQueryAccountBound(PredicateBuilder<T> builder, Class<T> entityClazz) {
        CriteriaBuilder cb = getEm().getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClazz);
        Root<T> root = cq.from(entityClazz);

        List<Predicate> predicates = builder.build(cb, root, cq);
        predicates.add(cb.equal(root.get(AccountBoundEntity_.accountId), security.get().getLoggedInAccount().getId()));

        cq.where(predicates.toArray(new Predicate[0]));
        cq.select(root);

        return new TypeQueryWrapper<>(getEm().createQuery(cq));
    }

}
