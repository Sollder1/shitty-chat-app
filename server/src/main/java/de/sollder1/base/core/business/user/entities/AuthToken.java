package de.sollder1.base.core.business.user.entities;

import de.sollder1.base.core.business.persitence.db.entities.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import java.util.Date;

@Entity(name = "auth_token")
public class AuthToken extends BaseEntity {

    private String token;
    @Column(name = "expiration_date")
    private Date expirationDate;
    //MAy be used for two phase auth...#
    @Column(name = "origin_ip")
    private String originIp;

    @JoinColumn(name = "account_id")
    @ManyToOne
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getOriginIp() {
        return originIp;
    }

    public void setOriginIp(String originIp) {
        this.originIp = originIp;
    }
}
