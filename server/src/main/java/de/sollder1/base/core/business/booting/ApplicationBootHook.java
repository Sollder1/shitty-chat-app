package de.sollder1.base.core.business.booting;

import de.sollder1.base.core.business.localization.internal.TranslationCache;
import de.sollder1.base.core.business.persitence.liquibase.LiquibaseManager;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;

import java.util.TimeZone;

@Startup
@Singleton
public class ApplicationBootHook {

    @Inject
    private TranslationCache translationCache;
    @Inject
    private LiquibaseManager liquibaseManager;
    @Inject
    @Any
    private Instance<CustomBootHook> bootHooks;

    //JPA is not working here.
    //Injections obviously do, as this hook is called just after the CDI env is ready.
    @PostConstruct
    private void init() {

        try {
            TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
            translationCache.init();
            liquibaseManager.startUpdate();
            bootHooks.forEach(CustomBootHook::execute);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(-1);
        }

    }

    @PreDestroy
    private void cleanUp() {
        // when app is undeployed, i.a the cdi env. is shutting down.
    }


}
