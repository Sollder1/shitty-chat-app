package de.sollder1.base.core.business.user.account;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import de.sollder1.base.core.business.user.entities.Gender;
import de.sollder1.base.core.business.user.entities.Person;
import de.sollder1.base.core.business.user.exception.AuthenticationException;
import de.sollder1.base.core.business.user.exception.EmailNotUniqueException;
import de.sollder1.base.core.business.user.security.impl.SecurityDao;
import de.sollder1.base.core.business.configuration.ConfigurationFacade;
import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import de.sollder1.base.core.business.user.entities.Account;
import de.sollder1.base.core.business.user.exception.AccountAlreadyExistsException;
import de.sollder1.base.core.business.user.exception.UsernameNotUniqueException;
import de.sollder1.base.core.business.user.security.impl.PasswordUtils;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.transaction.Transactional;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Singleton
public class AccountFacade {

    private static final HttpTransport TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new GsonFactory();

    @Inject
    private ConfigurationFacade configurationFacade;
    @Inject
    private JpaManager jpa;
    @Inject
    private SecurityDao securityDao;
    @Inject
    private AccountDao accountDao;

    @Transactional(Transactional.TxType.SUPPORTS)
    public List<Account> queryAccounts(String query) {
        return accountDao.queryAccounts(query);
    }

    @Transactional(Transactional.TxType.SUPPORTS)
    public Optional<Account> getAccount(String id) {
        return accountDao.getAccountById(id);
    }

    //0e928a95-cf16-4df8-af44-19e311d57eeb
    //
    @Transactional
    public Account updateAccount(Account account) {
        checkUsernameUnique(account);
        checkEmailUnique(account.getAssignedPerson());

        if (account.isChangedPassword()) {
            PasswordUtils.setPasswordAndSalt(account);
        }

        return jpa.merge(account);
    }

    @Transactional
    public void deleteAccount(String id) {
        jpa.delete(getAccount(id).orElseThrow());
    }

    @Transactional
    public Account registerAccount(Account account) {
        checkUsernameUnique(account);
        PasswordUtils.setPasswordAndSalt(account);
        account.setAssignedPerson(jpa.merge(new Person()));
        account.setUsername(account.getUsername().toLowerCase(Locale.ROOT));
        return jpa.merge(account);
    }

    @Transactional
    public Account registerAccountByGoogle(String googleAuthToken) {

        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(TRANSPORT, JSON_FACTORY)
                .setAudience(Collections.singletonList(configurationFacade.getItem("googleClientId").orElseThrow().asString()))
                .build();

        try {
            var idToken = verifier.verify(googleAuthToken);
            if (idToken == null) {
                throw new AuthenticationException("googleLoginFailedMessage");
            }

            String userId = idToken.getPayload().getSubject();
            securityDao.getAccountGoogleUserId(userId).ifPresent(account -> {
                throw new AccountAlreadyExistsException();
            });

            if (!((Boolean) idToken.getPayload().get("email_verified"))) {
                throw new GoogleEmailNotVerifiedException();
            }

            var account = new Account();
            account.setUsername((String) idToken.getPayload().get("email"));
            account.setGoogleUserId(userId);
            Person person = new Person();
            person.setEmail((String) idToken.getPayload().get("email"));
            person.setFirstname((String) idToken.getPayload().get("given_name"));
            person.setLastname((String) idToken.getPayload().get("family_name"));
            person.setGender(Gender.UNKNOWN);
            person.setProfilePictureUrl((String) idToken.getPayload().get("picture"));
            account.setAssignedPerson(person);

            return jpa.merge(account);
        } catch (GeneralSecurityException | IOException e) {
            throw new AuthenticationException("googleLoginFailedMessage");
        }
    }


    @Transactional(Transactional.TxType.SUPPORTS)
    private void checkUsernameUnique(Account account) {
        var query = jpa.getEm().createNativeQuery("SELECT count(username) FROM account where username = ? AND id <> ?;");
        query.setParameter(1, account.getUsername());
        query.setParameter(2, account.getId());

        long count = (long) query.getSingleResult();

        if (count > 0) {
            throw new UsernameNotUniqueException();
        }
    }

    @Transactional(Transactional.TxType.SUPPORTS)
    private void checkEmailUnique(Person person) {
        var query = jpa.getEm().createNativeQuery("SELECT count(email) FROM person where email = ? AND id <> ?;");
        query.setParameter(1, person.getEmail());
        query.setParameter(2, person.getId());

        long count = (long) query.getSingleResult();

        if (count > 0) {
            throw new EmailNotUniqueException();
        }
    }

}
