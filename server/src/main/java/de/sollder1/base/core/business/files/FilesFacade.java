package de.sollder1.base.core.business.files;

import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@ApplicationScoped
public class FilesFacade {

    @Inject
    private StoreAccessor storeAccessor;
    @Inject
    private JpaManager jpa;


    public FileEntity writeFileToStore(InputStream data, String fileName) throws IOException {
        return storeAccessor.createFile(data, fileName);
    }

    public File readFileFromStore(FileEntity entity) throws IOException {
        return storeAccessor.loadFile(entity.getStorePath());
    }

    public FileEntity getEntityById(String id) {
        return jpa.get(FileEntity.class, id).orElseThrow();
    }


}
