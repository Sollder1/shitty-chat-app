package de.sollder1.base.core.business.user.exception;

public class PasswordNotMatchException extends AuthenticationException {

    public PasswordNotMatchException() {
        super("PasswordNotMatchExceptionMessage");
    }
}
