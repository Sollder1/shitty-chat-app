package de.sollder1.base.core.business.localization.api;

public interface Translator {

    String get(String key, Object... params);

}
