package de.sollder1.base.core.business.user.role;

import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import jakarta.inject.Inject;

public class RoleDao {

    @Inject
    private JpaManager jpa;


    public JpaManager getJpa() {
        return jpa;
    }
}
