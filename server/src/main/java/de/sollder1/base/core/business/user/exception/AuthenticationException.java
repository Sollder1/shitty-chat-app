package de.sollder1.base.core.business.user.exception;

import de.sollder1.base.core.business.exceptions.ReportableException;

public class AuthenticationException extends ReportableException {
    public AuthenticationException(String message) {
        super(message);
    }
}
