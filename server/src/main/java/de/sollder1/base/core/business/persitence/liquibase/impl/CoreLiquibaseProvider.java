package de.sollder1.base.core.business.persitence.liquibase.impl;

import de.sollder1.base.core.business.persitence.liquibase.LiquibaseChangelogProvider;

public class CoreLiquibaseProvider implements LiquibaseChangelogProvider {
    @Override
    public String getChangeLogPath() {
        return "liquibase/core/changelog.xml";
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
