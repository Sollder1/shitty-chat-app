package de.sollder1.base.core.business.user.account;

import de.sollder1.base.core.business.user.entities.Account;

public interface AccountDeletionCallback {

    void performAction(Account account);

}
