package de.sollder1.base.core.business.logging;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

@WebListener
public class FlushLogsListener implements ServletContextListener {

    private static DbWriter LOGGING_WRITER;

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Start...");
        if (FlushLogsListener.LOGGING_WRITER != null) {
            LOGGING_WRITER.close();
        }
    }

    public static void registerLoggingWriter(DbWriter writer) {
        FlushLogsListener.LOGGING_WRITER = writer;
    }

}
