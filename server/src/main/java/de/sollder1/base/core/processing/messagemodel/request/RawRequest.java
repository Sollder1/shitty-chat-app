package de.sollder1.base.core.processing.messagemodel.request;

import jakarta.ws.rs.core.MultivaluedMap;


public class RawRequest {

    private MultivaluedMap<String, String> requestHeaders;
    private MultivaluedMap<String, String> requestPathParams;
    private MultivaluedMap<String, String> requestQueryParams;
    private Object payloadRaw;

    public MultivaluedMap<String, String> getRequestHeaders() {
        return requestHeaders;
    }

    public void setRequestHeaders(MultivaluedMap<String, String> requestHeaders) {
        this.requestHeaders = requestHeaders;
    }

    public MultivaluedMap<String, String> getRequestPathParams() {
        return requestPathParams;
    }

    public void setRequestPathParams(MultivaluedMap<String, String> requestPathParams) {
        this.requestPathParams = requestPathParams;
    }

    public MultivaluedMap<String, String> getRequestQueryParams() {
        return requestQueryParams;
    }

    public void setRequestQueryParams(MultivaluedMap<String, String> requestQueryParams) {
        this.requestQueryParams = requestQueryParams;
    }

    public Object getPayloadRaw() {
        return payloadRaw;
    }

    public void setPayloadRaw(Object payloadRaw) {
        this.payloadRaw = payloadRaw;
    }
}
