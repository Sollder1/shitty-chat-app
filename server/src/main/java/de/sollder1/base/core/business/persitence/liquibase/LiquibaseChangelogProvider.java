package de.sollder1.base.core.business.persitence.liquibase;

public interface LiquibaseChangelogProvider {

    String getChangeLogPath();

    int getOrder();

}
