package de.sollder1.base.core.business.user.account;

import de.sollder1.base.core.business.exceptions.ReportableException;

public class GoogleEmailNotVerifiedException extends ReportableException {

    public GoogleEmailNotVerifiedException() {
        super("googleEmailNotVerifiedExceptionMessage");
    }

}
