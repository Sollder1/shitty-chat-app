package de.sollder1.base.core.business.user.security;

import de.sollder1.base.core.business.user.entities.Account;
import de.sollder1.base.core.business.user.entities.AuthToken;
import de.sollder1.base.core.business.user.exception.AuthenticationException;
import de.sollder1.base.core.business.user.exception.NotLoggedInException;

public interface Security {

    AuthToken login(String username, String password) throws AuthenticationException;

    AuthToken loginWithGoogle(String googleAuthToken);

    void validateAuthToken(String token) throws AuthenticationException;

    void logout(boolean invalidateToken);

    boolean isLoggedIn();

    boolean isAdmin();

    boolean hasPermission(String permission) throws NotLoggedInException;

    Account getLoggedInAccount() throws NotLoggedInException;

}
