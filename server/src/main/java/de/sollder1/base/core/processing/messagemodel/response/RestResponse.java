package de.sollder1.base.core.processing.messagemodel.response;

public class RestResponse<T> {

    private int statusCode;
    private String error;
    private T payload;

    public RestResponse() {
        statusCode = 200;
    }

    public RestResponse(T payload) {
        this();
        this.payload = payload;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    //TODO: Not a 100 % correct, yet for now close enough!
    public boolean hasFailed() {
        return statusCode != 200;
    }
}
