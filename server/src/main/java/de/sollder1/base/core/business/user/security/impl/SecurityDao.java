package de.sollder1.base.core.business.user.security.impl;

import de.sollder1.base.core.business.persitence.db.jpa.JpaManager;
import de.sollder1.base.core.business.persitence.db.jpa.impl.AsyncSave;
import de.sollder1.base.core.business.user.entities.Account;
import de.sollder1.base.core.business.user.entities.AuthToken;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

@ApplicationScoped
public class SecurityDao {
    @Inject
    @AsyncSave
    private JpaManager jpa;

    public Optional<AuthToken> getAuthToken(String token) {

        var builder = jpa.buildTypeQuery((cb, root, cq) -> {
            return Arrays.asList(
                    cb.equal(root.get("token"), token),
                    cb.greaterThan(root.get("expirationDate"), new Date())
            );
        }, AuthToken.class);

        return builder.getOptionalSingleResult();
    }

    public Optional<AuthToken> getValidTokenForAccount(Account account) {
        var builder = jpa.buildTypeQuery((cb, root, cq) -> {
            return Arrays.asList(
                    cb.equal(root.get("account"), account),
                    cb.greaterThan(root.get("expirationDate"), new Date())
            );
        }, AuthToken.class);

        return builder.getOptionalSingleResult();
    }


    public Optional<Account> getAccountByUserName(String username) {
        return jpa.buildTypeQuery((cb, root, cq) -> Collections.singletonList(cb.equal(root.get("username"), username))
                , Account.class).getOptionalSingleResult();
    }


    public Optional<Account> getAccountGoogleUserId(String googleUserId) {
        return jpa.buildTypeQuery((cb, root, cq) -> Collections.singletonList(cb.equal(root.get("googleUserId"), googleUserId))
                , Account.class).getOptionalSingleResult();
    }


}