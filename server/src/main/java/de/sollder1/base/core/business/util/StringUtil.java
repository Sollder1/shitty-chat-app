package de.sollder1.base.core.business.util;

public class StringUtil {

    public static boolean isEmpty(String toCheck) {
        if (toCheck == null) {
            return true;
        }
        return "".equals(toCheck);
    }

    public static boolean isNotEmpty(String toCheck) {
        return !isEmpty(toCheck);
    }
}
