package de.sollder1.base.core.processing.messagemodel.defaults;

import de.sollder1.base.core.processing.messagemodel.response.RestResponse;

public class EmptyResponse extends RestResponse<EmptyPayload> {
}
