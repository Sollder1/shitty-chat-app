package de.sollder1.base.core.business.util;

import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {

    public static Long to(Date date) {
        if (date == null) {
            return null;
        }
        return date.getTime();
    }


    public static Date from(String time) {
        return from(Long.parseLong(time));
    }

    public static Date from(Long time) {
        if (time == null) {
            return null;
        }
        return new Date(time);
    }

    public static Date now() {
        return new Date();
    }

    public static Date nowWithoutTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }


    public static Date nowWithoutTime(int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

}
