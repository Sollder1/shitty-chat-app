package de.sollder1.base.core.business.persitence.db.jpa.impl;

import jakarta.enterprise.context.RequestScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Wraps around a EntityManager, effectely only creating one EntityManager for the whole
 * request.
 * THIS CLASS ONLY WORKS IN SYNCHRONISED ENVIRONMENTS, as it is bound to a specific HTTP request!
 */
@RequestScoped
public class RequestBoundJpaManager extends JpaManagerAdapter {

    @PersistenceContext(name = "yfm-jta")
    private EntityManager em;

    public EntityManager getEm() {
        return em;
    }
}
