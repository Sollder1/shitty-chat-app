package de.sollder1.base.core.business.persitence.db.jpa.impl;

import de.sollder1.base.core.business.persitence.db.entities.AccountBoundEntity;
import de.sollder1.base.core.business.persitence.db.jpa.PredicateBuilder;
import de.sollder1.base.core.business.persitence.db.jpa.TypeQueryWrapper;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@AsyncSave
public class AsyncJpaManager extends JpaManagerAdapter {

    @PersistenceContext(name = "yfm-jta")
    private EntityManager em;

    @Override
    public EntityManager getEm() {
        return em;
    }

    @Override
    public <T extends AccountBoundEntity> TypeQueryWrapper<T> buildTypeQueryAccountBound(PredicateBuilder<T> builder, Class<T> entityClazz) {
        throw new UnsupportedOperationException("No security Context in a background task!");
    }
}
