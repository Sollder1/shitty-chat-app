package de.sollder1.base.core.databinding.impl.databinder.json;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;

public class JsonMapperPool {

    private static final JsonMapper mapper = new JsonMapper();

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static JsonMapper getMapper() {
        return mapper;
    }
}
