package de.sollder1.base.core.databinding.api.databinder;

import de.sollder1.base.core.business.localization.api.TranslatorBundle;
import de.sollder1.base.core.business.state.RequestBoundState;
import de.sollder1.base.core.databinding.api.DataBindingException;
import de.sollder1.base.core.processing.exceptionhandling.ExceptionManager;
import de.sollder1.base.core.processing.processors.Processor;
import de.sollder1.base.core.business.booting.GlobalConstants;
import de.sollder1.base.core.business.localization.api.Translator;
import de.sollder1.base.core.databinding.impl.security.RestAuthHandler;
import de.sollder1.base.core.processing.messagemodel.request.RawRequest;
import de.sollder1.base.core.processing.messagemodel.request.RestRequest;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;

import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import java.util.Optional;

public class DataBindingHandler {

    @Inject
    private RestAuthHandler authHandler;
    @Inject
    private RequestBoundState requestBoundState;
    @Inject
    private ExceptionManager exceptionManager;
    @Inject
    @TranslatorBundle("auth")
    private Translator translator;

    private RequestDataBinder requestDataBinder;
    private ResponseDataBinder responseDataBinder;

    public <T extends Processor> Response bind(RequestDataBinder requestDataBinder, ResponseDataBinder responseDataBinder,
                                               RawRequest rawRequest, Class<T> processorClass) {
        try {
            try {
                this.requestDataBinder = requestDataBinder;
                this.responseDataBinder = responseDataBinder;

                var processor = CDI.current().select(processorClass).get();
                var request = buildRequest(rawRequest, processor.getRequestType());
                handlePreProcessing(processor, request);
                var response = processor.processAndCatch(request, exceptionManager);

                return responseDataBinder.bind(response);
            } catch (DataBindingException e) {
                e.printStackTrace();
                return responseDataBinder.bindError(e.getResponseCode(), translator.get(e.getError()));
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return responseDataBinder.bindError(500);
        }
    }


    private RestRequest buildRequest(RawRequest rawRequest, Class<? extends RestRequest> requestType) throws DataBindingException {

        try {
            RestRequest request = requestType.getDeclaredConstructor().newInstance();
            request.setRawRequest(rawRequest);
            request.setPayload(requestDataBinder.bind(request));
            return request;

        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            throw new DataBindingException(500, "CouldNotGenerateRequest");
        }

    }

    private <T extends Processor> void handlePreProcessing(T processor, RestRequest request) throws DataBindingException {

        Optional<Locale> localeOptional = request.getHeaderOptional("Selected-Language").map(s -> new Locale((String) s));
        requestBoundState.setRequestLocale(localeOptional.orElse(GlobalConstants.DEFAULT_LOCALE));

        if (processor.authenticate()) {
            authHandler.authenticate(request);
        }

        if (processor.authorize().isPresent()) {
            authHandler.authorize((String) processor.authorize().get());
        }

        if (processor.mustBeAdmin()) {
            authHandler.isAdmin();
        }
    }


}
